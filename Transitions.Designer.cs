﻿namespace ikaa_8_ls_171rdb133
{
    partial class Transitions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.picture1 = new System.Windows.Forms.PictureBox();
            this.picture2 = new System.Windows.Forms.PictureBox();
            this.resultPicture = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.groupBoxOpacity = new System.Windows.Forms.GroupBox();
            this.opacityButton = new System.Windows.Forms.Button();
            this.opacityValue = new System.Windows.Forms.Label();
            this.opacityLabel = new System.Windows.Forms.Label();
            this.opacityTrackBar = new System.Windows.Forms.TrackBar();
            this.groupBoxEffects = new System.Windows.Forms.GroupBox();
            this.differenceButton = new System.Windows.Forms.Button();
            this.overlayButton = new System.Windows.Forms.Button();
            this.burnButton = new System.Windows.Forms.Button();
            this.subtractionButton = new System.Windows.Forms.Button();
            this.lightenButton = new System.Windows.Forms.Button();
            this.hardLightButton = new System.Windows.Forms.Button();
            this.darkenButton = new System.Windows.Forms.Button();
            this.multiplyButton = new System.Windows.Forms.Button();
            this.softLightButton = new System.Windows.Forms.Button();
            this.dodgeButton = new System.Windows.Forms.Button();
            this.additionButton = new System.Windows.Forms.Button();
            this.screenButton = new System.Windows.Forms.Button();
            this.groupBoxTransitions = new System.Windows.Forms.GroupBox();
            this.squareRadio = new System.Windows.Forms.RadioButton();
            this.circleRadio = new System.Windows.Forms.RadioButton();
            this.swipeRadio = new System.Windows.Forms.RadioButton();
            this.wipeRadio = new System.Windows.Forms.RadioButton();
            this.fadeRadio = new System.Windows.Forms.RadioButton();
            this.transitionButton = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultPicture)).BeginInit();
            this.groupBoxOpacity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.opacityTrackBar)).BeginInit();
            this.groupBoxEffects.SuspendLayout();
            this.groupBoxTransitions.SuspendLayout();
            this.SuspendLayout();
            // 
            // picture1
            // 
            this.picture1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picture1.Location = new System.Drawing.Point(12, 12);
            this.picture1.Name = "picture1";
            this.picture1.Size = new System.Drawing.Size(275, 219);
            this.picture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picture1.TabIndex = 0;
            this.picture1.TabStop = false;
            this.picture1.Click += new System.EventHandler(this.picture1_Click);
            // 
            // picture2
            // 
            this.picture2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picture2.Location = new System.Drawing.Point(654, 12);
            this.picture2.Name = "picture2";
            this.picture2.Size = new System.Drawing.Size(275, 219);
            this.picture2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picture2.TabIndex = 1;
            this.picture2.TabStop = false;
            this.picture2.Click += new System.EventHandler(this.picture2_Click);
            // 
            // resultPicture
            // 
            this.resultPicture.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.resultPicture.Location = new System.Drawing.Point(288, 240);
            this.resultPicture.Name = "resultPicture";
            this.resultPicture.Size = new System.Drawing.Size(366, 269);
            this.resultPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.resultPicture.TabIndex = 2;
            this.resultPicture.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // groupBoxOpacity
            // 
            this.groupBoxOpacity.Controls.Add(this.opacityButton);
            this.groupBoxOpacity.Controls.Add(this.opacityValue);
            this.groupBoxOpacity.Controls.Add(this.opacityLabel);
            this.groupBoxOpacity.Controls.Add(this.opacityTrackBar);
            this.groupBoxOpacity.Location = new System.Drawing.Point(293, 12);
            this.groupBoxOpacity.Name = "groupBoxOpacity";
            this.groupBoxOpacity.Size = new System.Drawing.Size(355, 101);
            this.groupBoxOpacity.TabIndex = 3;
            this.groupBoxOpacity.TabStop = false;
            this.groupBoxOpacity.Text = "Opacity";
            // 
            // opacityButton
            // 
            this.opacityButton.Location = new System.Drawing.Point(106, 19);
            this.opacityButton.Name = "opacityButton";
            this.opacityButton.Size = new System.Drawing.Size(89, 23);
            this.opacityButton.TabIndex = 3;
            this.opacityButton.Text = "Apply Opacity";
            this.opacityButton.UseVisualStyleBackColor = true;
            this.opacityButton.Click += new System.EventHandler(this.opacityButton_Click);
            // 
            // opacityValue
            // 
            this.opacityValue.AutoSize = true;
            this.opacityValue.Location = new System.Drawing.Point(66, 20);
            this.opacityValue.Name = "opacityValue";
            this.opacityValue.Size = new System.Drawing.Size(21, 13);
            this.opacityValue.TabIndex = 2;
            this.opacityValue.Text = "0%";
            // 
            // opacityLabel
            // 
            this.opacityLabel.AutoSize = true;
            this.opacityLabel.Location = new System.Drawing.Point(7, 20);
            this.opacityLabel.Name = "opacityLabel";
            this.opacityLabel.Size = new System.Drawing.Size(52, 13);
            this.opacityLabel.TabIndex = 1;
            this.opacityLabel.Text = "Opacity =";
            // 
            // opacityTrackBar
            // 
            this.opacityTrackBar.Location = new System.Drawing.Point(7, 50);
            this.opacityTrackBar.Maximum = 100;
            this.opacityTrackBar.Name = "opacityTrackBar";
            this.opacityTrackBar.Size = new System.Drawing.Size(342, 45);
            this.opacityTrackBar.TabIndex = 0;
            this.opacityTrackBar.Scroll += new System.EventHandler(this.opacityTrackBar_Scroll);
            // 
            // groupBoxEffects
            // 
            this.groupBoxEffects.Controls.Add(this.differenceButton);
            this.groupBoxEffects.Controls.Add(this.overlayButton);
            this.groupBoxEffects.Controls.Add(this.burnButton);
            this.groupBoxEffects.Controls.Add(this.subtractionButton);
            this.groupBoxEffects.Controls.Add(this.lightenButton);
            this.groupBoxEffects.Controls.Add(this.hardLightButton);
            this.groupBoxEffects.Controls.Add(this.darkenButton);
            this.groupBoxEffects.Controls.Add(this.multiplyButton);
            this.groupBoxEffects.Controls.Add(this.softLightButton);
            this.groupBoxEffects.Controls.Add(this.dodgeButton);
            this.groupBoxEffects.Controls.Add(this.additionButton);
            this.groupBoxEffects.Controls.Add(this.screenButton);
            this.groupBoxEffects.Location = new System.Drawing.Point(292, 119);
            this.groupBoxEffects.Name = "groupBoxEffects";
            this.groupBoxEffects.Size = new System.Drawing.Size(355, 111);
            this.groupBoxEffects.TabIndex = 4;
            this.groupBoxEffects.TabStop = false;
            this.groupBoxEffects.Text = "Effects";
            // 
            // differenceButton
            // 
            this.differenceButton.Location = new System.Drawing.Point(249, 77);
            this.differenceButton.Name = "differenceButton";
            this.differenceButton.Size = new System.Drawing.Size(75, 23);
            this.differenceButton.TabIndex = 16;
            this.differenceButton.Text = "Difference";
            this.differenceButton.UseVisualStyleBackColor = true;
            this.differenceButton.Click += new System.EventHandler(this.differenceButton_Click);
            // 
            // overlayButton
            // 
            this.overlayButton.Location = new System.Drawing.Point(168, 19);
            this.overlayButton.Name = "overlayButton";
            this.overlayButton.Size = new System.Drawing.Size(75, 23);
            this.overlayButton.TabIndex = 15;
            this.overlayButton.Text = "Overlay";
            this.overlayButton.UseVisualStyleBackColor = true;
            this.overlayButton.Click += new System.EventHandler(this.overlayButton_Click);
            // 
            // burnButton
            // 
            this.burnButton.Location = new System.Drawing.Point(249, 19);
            this.burnButton.Name = "burnButton";
            this.burnButton.Size = new System.Drawing.Size(75, 23);
            this.burnButton.TabIndex = 14;
            this.burnButton.Text = "Burn";
            this.burnButton.UseVisualStyleBackColor = true;
            this.burnButton.Click += new System.EventHandler(this.burnButton_Click);
            // 
            // subtractionButton
            // 
            this.subtractionButton.Location = new System.Drawing.Point(87, 77);
            this.subtractionButton.Name = "subtractionButton";
            this.subtractionButton.Size = new System.Drawing.Size(75, 23);
            this.subtractionButton.TabIndex = 13;
            this.subtractionButton.Text = "Subtraction";
            this.subtractionButton.UseVisualStyleBackColor = true;
            this.subtractionButton.Click += new System.EventHandler(this.subtractionButton_Click);
            // 
            // lightenButton
            // 
            this.lightenButton.Location = new System.Drawing.Point(168, 48);
            this.lightenButton.Name = "lightenButton";
            this.lightenButton.Size = new System.Drawing.Size(75, 23);
            this.lightenButton.TabIndex = 12;
            this.lightenButton.Text = "Lighten";
            this.lightenButton.UseVisualStyleBackColor = true;
            this.lightenButton.Click += new System.EventHandler(this.lightenButton_Click);
            // 
            // hardLightButton
            // 
            this.hardLightButton.Location = new System.Drawing.Point(87, 48);
            this.hardLightButton.Name = "hardLightButton";
            this.hardLightButton.Size = new System.Drawing.Size(75, 23);
            this.hardLightButton.TabIndex = 10;
            this.hardLightButton.Text = "Hard Light";
            this.hardLightButton.UseVisualStyleBackColor = true;
            this.hardLightButton.Click += new System.EventHandler(this.hardLightButton_Click);
            // 
            // darkenButton
            // 
            this.darkenButton.Location = new System.Drawing.Point(249, 48);
            this.darkenButton.Name = "darkenButton";
            this.darkenButton.Size = new System.Drawing.Size(75, 23);
            this.darkenButton.TabIndex = 11;
            this.darkenButton.Text = "Darken";
            this.darkenButton.UseVisualStyleBackColor = true;
            this.darkenButton.Click += new System.EventHandler(this.darkenButton_Click);
            // 
            // multiplyButton
            // 
            this.multiplyButton.Location = new System.Drawing.Point(168, 77);
            this.multiplyButton.Name = "multiplyButton";
            this.multiplyButton.Size = new System.Drawing.Size(75, 23);
            this.multiplyButton.TabIndex = 9;
            this.multiplyButton.Text = "Multipy";
            this.multiplyButton.UseVisualStyleBackColor = true;
            this.multiplyButton.Click += new System.EventHandler(this.multiplyButton_Click);
            // 
            // softLightButton
            // 
            this.softLightButton.Location = new System.Drawing.Point(6, 48);
            this.softLightButton.Name = "softLightButton";
            this.softLightButton.Size = new System.Drawing.Size(75, 23);
            this.softLightButton.TabIndex = 7;
            this.softLightButton.Text = "Soft Light";
            this.softLightButton.UseVisualStyleBackColor = true;
            this.softLightButton.Click += new System.EventHandler(this.softLightButton_Click);
            // 
            // dodgeButton
            // 
            this.dodgeButton.Location = new System.Drawing.Point(87, 19);
            this.dodgeButton.Name = "dodgeButton";
            this.dodgeButton.Size = new System.Drawing.Size(75, 23);
            this.dodgeButton.TabIndex = 8;
            this.dodgeButton.Text = "Dodge";
            this.dodgeButton.UseVisualStyleBackColor = true;
            this.dodgeButton.Click += new System.EventHandler(this.dodgeButton_Click);
            // 
            // additionButton
            // 
            this.additionButton.Location = new System.Drawing.Point(8, 77);
            this.additionButton.Name = "additionButton";
            this.additionButton.Size = new System.Drawing.Size(75, 23);
            this.additionButton.TabIndex = 6;
            this.additionButton.Text = "Addition";
            this.additionButton.UseVisualStyleBackColor = true;
            this.additionButton.Click += new System.EventHandler(this.additionButton_Click);
            // 
            // screenButton
            // 
            this.screenButton.Location = new System.Drawing.Point(6, 19);
            this.screenButton.Name = "screenButton";
            this.screenButton.Size = new System.Drawing.Size(75, 23);
            this.screenButton.TabIndex = 5;
            this.screenButton.Text = "Screen";
            this.screenButton.UseVisualStyleBackColor = true;
            this.screenButton.Click += new System.EventHandler(this.screenButton_Click);
            // 
            // groupBoxTransitions
            // 
            this.groupBoxTransitions.Controls.Add(this.squareRadio);
            this.groupBoxTransitions.Controls.Add(this.circleRadio);
            this.groupBoxTransitions.Controls.Add(this.swipeRadio);
            this.groupBoxTransitions.Controls.Add(this.wipeRadio);
            this.groupBoxTransitions.Controls.Add(this.fadeRadio);
            this.groupBoxTransitions.Controls.Add(this.transitionButton);
            this.groupBoxTransitions.Location = new System.Drawing.Point(664, 332);
            this.groupBoxTransitions.Name = "groupBoxTransitions";
            this.groupBoxTransitions.Size = new System.Drawing.Size(265, 177);
            this.groupBoxTransitions.TabIndex = 5;
            this.groupBoxTransitions.TabStop = false;
            this.groupBoxTransitions.Text = "Transition";
            // 
            // squareRadio
            // 
            this.squareRadio.AutoSize = true;
            this.squareRadio.Location = new System.Drawing.Point(7, 112);
            this.squareRadio.Name = "squareRadio";
            this.squareRadio.Size = new System.Drawing.Size(93, 17);
            this.squareRadio.TabIndex = 5;
            this.squareRadio.Text = "Shape Square";
            this.squareRadio.UseVisualStyleBackColor = true;
            // 
            // circleRadio
            // 
            this.circleRadio.AutoSize = true;
            this.circleRadio.Location = new System.Drawing.Point(7, 89);
            this.circleRadio.Name = "circleRadio";
            this.circleRadio.Size = new System.Drawing.Size(85, 17);
            this.circleRadio.TabIndex = 4;
            this.circleRadio.Text = "Shape Circle";
            this.circleRadio.UseVisualStyleBackColor = true;
            // 
            // swipeRadio
            // 
            this.swipeRadio.AutoSize = true;
            this.swipeRadio.Location = new System.Drawing.Point(7, 66);
            this.swipeRadio.Name = "swipeRadio";
            this.swipeRadio.Size = new System.Drawing.Size(54, 17);
            this.swipeRadio.TabIndex = 3;
            this.swipeRadio.Text = "Swipe";
            this.swipeRadio.UseVisualStyleBackColor = true;
            // 
            // wipeRadio
            // 
            this.wipeRadio.AutoSize = true;
            this.wipeRadio.Location = new System.Drawing.Point(7, 43);
            this.wipeRadio.Name = "wipeRadio";
            this.wipeRadio.Size = new System.Drawing.Size(50, 17);
            this.wipeRadio.TabIndex = 2;
            this.wipeRadio.Text = "Wipe";
            this.wipeRadio.UseVisualStyleBackColor = true;
            // 
            // fadeRadio
            // 
            this.fadeRadio.AutoSize = true;
            this.fadeRadio.Checked = true;
            this.fadeRadio.Location = new System.Drawing.Point(7, 20);
            this.fadeRadio.Name = "fadeRadio";
            this.fadeRadio.Size = new System.Drawing.Size(49, 17);
            this.fadeRadio.TabIndex = 1;
            this.fadeRadio.TabStop = true;
            this.fadeRadio.Text = "Fade";
            this.fadeRadio.UseVisualStyleBackColor = true;
            // 
            // transitionButton
            // 
            this.transitionButton.Location = new System.Drawing.Point(6, 135);
            this.transitionButton.Name = "transitionButton";
            this.transitionButton.Size = new System.Drawing.Size(253, 32);
            this.transitionButton.TabIndex = 0;
            this.transitionButton.Text = "Show Transition";
            this.transitionButton.UseVisualStyleBackColor = true;
            this.transitionButton.Click += new System.EventHandler(this.transitionButton_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 518);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(916, 30);
            this.progressBar.TabIndex = 6;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Transitions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 552);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.groupBoxTransitions);
            this.Controls.Add(this.groupBoxEffects);
            this.Controls.Add(this.groupBoxOpacity);
            this.Controls.Add(this.resultPicture);
            this.Controls.Add(this.picture2);
            this.Controls.Add(this.picture1);
            this.Name = "Transitions";
            this.Text = "Transitions";
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultPicture)).EndInit();
            this.groupBoxOpacity.ResumeLayout(false);
            this.groupBoxOpacity.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.opacityTrackBar)).EndInit();
            this.groupBoxEffects.ResumeLayout(false);
            this.groupBoxTransitions.ResumeLayout(false);
            this.groupBoxTransitions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picture1;
        private System.Windows.Forms.PictureBox picture2;
        private System.Windows.Forms.PictureBox resultPicture;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.GroupBox groupBoxOpacity;
        private System.Windows.Forms.Button opacityButton;
        private System.Windows.Forms.Label opacityValue;
        private System.Windows.Forms.Label opacityLabel;
        private System.Windows.Forms.TrackBar opacityTrackBar;
        private System.Windows.Forms.GroupBox groupBoxEffects;
        private System.Windows.Forms.Button differenceButton;
        private System.Windows.Forms.Button overlayButton;
        private System.Windows.Forms.Button burnButton;
        private System.Windows.Forms.Button subtractionButton;
        private System.Windows.Forms.Button lightenButton;
        private System.Windows.Forms.Button hardLightButton;
        private System.Windows.Forms.Button darkenButton;
        private System.Windows.Forms.Button multiplyButton;
        private System.Windows.Forms.Button softLightButton;
        private System.Windows.Forms.Button dodgeButton;
        private System.Windows.Forms.Button additionButton;
        private System.Windows.Forms.Button screenButton;
        private System.Windows.Forms.GroupBox groupBoxTransitions;
        private System.Windows.Forms.RadioButton squareRadio;
        private System.Windows.Forms.RadioButton circleRadio;
        private System.Windows.Forms.RadioButton swipeRadio;
        private System.Windows.Forms.RadioButton wipeRadio;
        private System.Windows.Forms.RadioButton fadeRadio;
        private System.Windows.Forms.Button transitionButton;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Timer timer1;
    }
}