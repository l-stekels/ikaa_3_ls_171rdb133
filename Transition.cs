﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ikaa_8_ls_171rdb133
{
    public class Transition
    {
        private string type;
        private PixelClassRGB[,] image1;
        private PixelClassRGB[,] image2;

        public Transition(string type, PixelClassRGB[,] image1, PixelClassRGB[,] image2) => (this.type, this.image1, this.image2) = (type, image1, image2);
        
        public PixelClassRGB GetPixel(int step, int x, int y)
        {
            PixelClassRGB pixel1 = image1[x, y];
            PixelClassRGB pixel2 = image2[x, y];
            int height = image1.GetLength(0);
            int width = image1.GetLength(1);
            byte r, g, b;
            switch (type)
            {
                case "WIPE":
                    if (x <= (height * step / 100))
                    {
                        r = pixel2.R;
                        g = pixel2.G;
                        b = pixel2.B;
                    } else
                    {
                        r = pixel1.R;
                        g = pixel1.G;
                        b = pixel1.B;
                    }
                    break;
                case "SWIPE":
                    if (Math.Abs(x - (height / 2)) < ((height * step) / 200))
                    {
                        r = pixel2.R;
                        g = pixel2.G;
                        b = pixel2.B;
                    } else
                    {
                        r = pixel1.R;
                        g = pixel1.G;
                        b = pixel1.B;
                    }
                    break;
                case "CIRCLE":
                    if ((Math.Pow(y - (width / 2), 2.0) + Math.Pow(x - (height / 2), 2.0)) < (((height * step) / 100) * ((width * step) / 100)))
                    {
                        r = pixel2.R;
                        g = pixel2.G;
                        b = pixel2.B;
                    }
                    else
                    {
                        r = pixel1.R;
                        g = pixel1.G;
                        b = pixel1.B;
                    }
                    break;
                case "SQUARE":
                    if (Math.Abs(y - (width / 2)) < ((width * step) / 200) && Math.Abs(x - (height / 2)) < ((height * step) / 200))
                    {
                        r = pixel2.R;
                        g = pixel2.G;
                        b = pixel2.B;
                    }
                    else
                    {
                        r = pixel1.R;
                        g = pixel1.G;
                        b = pixel1.B;
                    }
                    break;
                default:
                    r = (byte)(((step / 100.0) * pixel2.R) + ((1.0 - (step / 100.0)) * pixel1.R));
                    g = (byte)(((step / 100.0) * pixel2.G) + ((1.0 - (step / 100.0)) * pixel1.G));
                    b = (byte)(((step / 100.0) * pixel2.B) + ((1.0 - (step / 100.0)) * pixel1.B));
                    break;
            }
            return new PixelClassRGB(r, g, b);
        }
    }
}
