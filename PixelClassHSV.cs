﻿using System;

namespace ikaa_8_ls_171rdb133
{
    public class PixelClassHSV
    {
        public int H; // Hue
        public byte S; // Saturation
        public byte V; // Value

        public PixelClassHSV()
        {
            H = 0;
            S = 0;
            V = 0;
        }

        public PixelClassHSV(byte R, byte G, byte B)
        {
            int MAX = Math.Max(R, Math.Max(G, B));
            int MIN = Math.Min(R, Math.Min(G, B));

            if (MAX == MIN)
            {
                H = 0;
            }
            else if (MAX == R && G >= B)
            {
                H = 60 * (G - B) / (MAX - MIN);
            }
            else if (MAX == R && G < B)
            {
                H = 60 * (G - B) / (MAX - MIN) + 360;
            }
            else if (MAX == G)
            {
                H = 60 * (B - R) / (MAX - MIN) + 120;
            }
            else
            {
                H = 60 * (R - G) / (MAX - MIN) + 240;
            }

            if (H == 360)
            {
                H = 0;
            }

            if (MAX == 0)
            {
                S = 0;
            }
            else
            {
                S = Convert.ToByte(255 * (1 - ((float)MIN / MAX)));
            }
            V = (byte)MAX;
        }
    }
}
