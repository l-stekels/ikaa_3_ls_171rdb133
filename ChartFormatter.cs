﻿using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;

namespace ikaa_8_ls_171rdb133
{
    public class ChartFormatter
    {
        public ImgData imgData;

        public string mode;

        public ChartFormatter(ImgData imgData, string mode)
        {
            this.imgData = imgData;
            this.mode = mode;
        }

        public void DrawChart(Chart optionalChart = null)
        {
            if (optionalChart != null)
            {
                imgData.chart = optionalChart;
            }
            imgData.chart.Series.Clear();
            switch (mode)
            {
                case "RED":
                    DrawRed();
                    break;
                case "GREEN":
                    DrawGreen();
                    break;
                case "BLUE":
                    DrawBlue();
                    break;
                case "INTENSITY":
                    DrawIntensity();
                    break;
                case "HSV":
                    DrawHSV();
                    break;
                case "HUE":
                    DrawHue();
                    break;
                case "SATURATION":
                    DrawSaturation();
                    break;
                case "VALUE":
                    DrawValue();
                    break;
                default:
                    DrawRGB();
                    break;
            }
        }

        protected void DrawValue()
        {
            imgData.chart.Series.Add("Value");
            imgData.chart.Series["Value"].Color = Color.DarkGray;
            imgData.chart.Series["Value"].Points.DataBindY(imgData.stretch(imgData.histoValue));
        }

        protected void DrawSaturation()
        {
            imgData.chart.Series.Add("Saturation");
            imgData.chart.Series["Saturation"].Color = Color.Orange;
            imgData.chart.Series["Saturation"].Points.DataBindY(imgData.stretch(imgData.histoSaturation));
        }

        protected void DrawHue()
        {
            imgData.chart.Series.Add("Hue");
            imgData.chart.Series["Hue"].Color = Color.OrangeRed;
            imgData.chart.Series["Hue"].Points.DataBindY(imgData.stretch(imgData.histoHue));
        }

        protected void DrawHSV()
        {
            DrawHue();
            DrawSaturation();
            DrawValue();
        }

        protected void DrawRGB()
        {
            DrawRed();
            DrawGreen();
            DrawBlue();
            DrawIntensity();
        }

        protected void DrawRed()
        {
            imgData.chart.Series.Add("Red");
            imgData.chart.Series["Red"].Color = Color.Red;
            imgData.chart.Series["Red"].Points.DataBindY(imgData.stretch(imgData.histoRed));
        }

        protected void DrawGreen()
        {
            imgData.chart.Series.Add("Green");
            imgData.chart.Series["Green"].Color = Color.Green;
            imgData.chart.Series["Green"].Points.DataBindY(imgData.stretch(imgData.histoGreen));
        }

        protected void DrawBlue()
        {
            imgData.chart.Series.Add("Blue");
            imgData.chart.Series["Blue"].Color = Color.Blue;
            imgData.chart.Series["Blue"].Points.DataBindY(imgData.stretch(imgData.histoBlue));
        }

        protected void DrawIntensity()
        {
            imgData.chart.Series.Add("Intensity");
            imgData.chart.Series["Intensity"].Color = Color.Gray;
            imgData.chart.Series["Intensity"].Points.DataBindY(imgData.stretch(imgData.histoIntensity));
        }
    }
}
