﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ikaa_8_ls_171rdb133
{
    public class Filter
    {
        public int[,] filter;
        public int K;
        public int size;
        public bool median;

        public Filter(int size = 3, bool median = false)
        {
            this.size = size;
            filter = new int[size, size];
            this.median = median;
        }

        public Filter Blur3x3(int k)
        {
            K = k;
            if (K == 9)
            {
                filter = new int[,] { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
            }
            if (K == 10)
            {
                filter = new int[,] { { 1, 1, 1 }, { 1, 2, 1 }, { 1, 1, 1 } };
            }
            if (K == 16)
            {
                filter = new int[,] { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };
            }
            return this;
        }

        public Filter Sharpen3x3(string type)
        {
            K = 1;
            if (type == "-1 5 -1")
            {
                filter = new int[,] { { 0, -1, 0 }, { -1, 5, -1 }, { 0, -1, 0 } };
            }
            if (type == "-1 9 -1")
            {
                filter = new int[,] { { -1, -1, -1 }, { -1, 9, -1 }, { -1, -1, -1 } };
            }
            if (type == "-2 9 -2")
            {
                filter = new int[,] { { 0, -2, 0 }, { -2, 9, -2 }, { 0, -2, 0 } };
            }
            return this;
        }
    }
}
