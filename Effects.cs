﻿using System;

namespace ikaa_8_ls_171rdb133
{
    public class Effects
    {
        public Transitions form;
        public Effects(Transitions form) => (this.form) = (form);

        public void Opacity(int opacityValue)
        {
            double opacity = (double)opacityValue / 100;
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];
                    byte r = (byte)(opacity * secondImagePixel.R + (1 - opacity) * firstImagePixel.R);
                    byte g = (byte)(opacity * secondImagePixel.G + (1 - opacity) * firstImagePixel.G);
                    byte b = (byte)(opacity * secondImagePixel.B + (1 - opacity) * firstImagePixel.B);
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void Screen()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];
                    byte r = (byte)Math.Min(255, (1 - (1 - (double)firstImagePixel.R / 255) * (1 - (double)secondImagePixel.R / 255)) * 255);
                    byte g = (byte)Math.Min(255, (1 - (1 - (double)firstImagePixel.G / 255) * (1 - (double)secondImagePixel.G / 255)) * 255);
                    byte b = (byte)Math.Min(255, (1 - (1 - (double)firstImagePixel.B / 255) * (1 - (double)secondImagePixel.B / 255)) * 255);
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void Dodge()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];
                    double Ar = (double)firstImagePixel.R / 255;
                    double Ag = (double)firstImagePixel.G / 255;
                    double Ab = (double)firstImagePixel.B / 255;
                    double Br = (double)secondImagePixel.R / 255;
                    double Bg = (double)secondImagePixel.G / 255;
                    double Bb = (double)secondImagePixel.B / 255;
                    Ar = Ar == 1.0 ? 0.0 : Ar;
                    Ag = Ag == 1.0 ? 0.0 : Ag;
                    Ab = Ab == 1.0 ? 0.0 : Ab;
                    byte r = (byte)Math.Max(0, Math.Min(255, Br / (1 - Ar) * 255));
                    byte g = (byte)Math.Max(0, Math.Min(255, Bg / (1 - Ag) * 255));
                    byte b = (byte)Math.Max(0, Math.Min(255, Bb / (1 - Ab) * 255));
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void Difference()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];
                    byte r = (byte)Math.Abs(firstImagePixel.R - secondImagePixel.R);
                    byte g = (byte)Math.Abs(firstImagePixel.G - secondImagePixel.G);
                    byte b = (byte)Math.Abs(firstImagePixel.B - secondImagePixel.B);
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void Multipy()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];
                    double Ar = (double)firstImagePixel.R / 255;
                    double Ag = (double)firstImagePixel.G / 255;
                    double Ab = (double)firstImagePixel.B / 255;
                    double Br = (double)secondImagePixel.R / 255;
                    double Bg = (double)secondImagePixel.G / 255;
                    double Bb = (double)secondImagePixel.B / 255;

                    byte r = (byte)Math.Max(0, Math.Min(255, Ar * Br * 255));
                    byte g = (byte)Math.Max(0, Math.Min(255, Ag * Bg * 255));
                    byte b = (byte)Math.Max(0, Math.Min(255, Ab * Bb * 255));
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void Overlay()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];
                    double Ar = (double)firstImagePixel.R / 255;
                    double Ag = (double)firstImagePixel.G / 255;
                    double Ab = (double)firstImagePixel.B / 255;
                    double Br = (double)secondImagePixel.R / 255;
                    double Bg = (double)secondImagePixel.G / 255;
                    double Bb = (double)secondImagePixel.B / 255;

                    byte r = (byte)Math.Min(255, Br <= 0.5 ? (2 * Ar * Br) * 255 : (1 - 2 * (1 - Ar) * (1 - Br)) * 255);
                    byte g = (byte)Math.Min(255, Bg <= 0.5 ? (2 * Ag * Bg) * 255 : (1 - 2 * (1 - Ag) * (1 - Bg)) * 255);
                    byte b = (byte)Math.Min(255, Bb <= 0.5 ? (2 * Ab * Bb) * 255 : (1 - 2 * (1 - Ab) * (1 - Bb)) * 255);
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void Subtraction()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];

                    byte r = (byte)Math.Max(0, Math.Min(255, firstImagePixel.R + secondImagePixel.R - 255));
                    byte g = (byte)Math.Max(0, Math.Min(255, firstImagePixel.G + secondImagePixel.G - 255));
                    byte b = (byte)Math.Max(0, Math.Min(255, firstImagePixel.B + secondImagePixel.B - 255));
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void Addition()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];

                    byte r = (byte)Math.Max(0, Math.Min(255, firstImagePixel.R + secondImagePixel.R));
                    byte g = (byte)Math.Max(0, Math.Min(255, firstImagePixel.G + secondImagePixel.G));
                    byte b = (byte)Math.Max(0, Math.Min(255, firstImagePixel.B + secondImagePixel.B));
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void Darken()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];

                    byte r = secondImagePixel.R >= firstImagePixel.R ? firstImagePixel.R : secondImagePixel.R;
                    byte g = secondImagePixel.G >= firstImagePixel.G ? firstImagePixel.G : secondImagePixel.G;
                    byte b = secondImagePixel.B >= firstImagePixel.B ? firstImagePixel.B : secondImagePixel.B;
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void Burn()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];
                    double Ar = (double)firstImagePixel.R / 255;
                    double Ag = (double)firstImagePixel.G / 255;
                    double Ab = (double)firstImagePixel.B / 255;
                    double Br = (double)secondImagePixel.R / 255;
                    double Bg = (double)secondImagePixel.G / 255;
                    double Bb = (double)secondImagePixel.B / 255;
                    Ar = Ar == 0.0 ? 1.0 : Ar;
                    Ag = Ag == 0.0 ? 1.0 : Ag;
                    Ab = Ab == 0.0 ? 1.0 : Ab;
                    byte r = (byte)Math.Max(0, Math.Min(255, (1 - ((1 - Br) / Ar)) * 255));
                    byte g = (byte)Math.Max(0, Math.Min(255, (1 - ((1 - Bg) / Ag)) * 255));
                    byte b = (byte)Math.Max(0, Math.Min(255, (1 - ((1 - Bb) / Ab)) * 255));
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void Lighten()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];

                    byte r = secondImagePixel.R >= firstImagePixel.R ? secondImagePixel.R : firstImagePixel.R;
                    byte g = secondImagePixel.G >= firstImagePixel.G ? secondImagePixel.G : firstImagePixel.G;
                    byte b = secondImagePixel.B >= firstImagePixel.B ? secondImagePixel.B : firstImagePixel.B;
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void HardLight()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];
                    double Ar = (double)firstImagePixel.R / 255;
                    double Ag = (double)firstImagePixel.G / 255;
                    double Ab = (double)firstImagePixel.B / 255;
                    double Br = (double)secondImagePixel.R / 255;
                    double Bg = (double)secondImagePixel.G / 255;
                    double Bb = (double)secondImagePixel.B / 255;

                    byte r = (byte)(Ar <= 0.5 ? (2 * Ar * Br) * 255 : (1 - 2 * (1 - Ar) * (1 - Br)) * 255);
                    byte g = (byte)(Ag <= 0.5 ? (2 * Ag * Bg) * 255 : (1 - 2 * (1 - Ag) * (1 - Bg)) * 255);
                    byte b = (byte)(Ab <= 0.5 ? (2 * Ab * Bb) * 255 : (1 - 2 * (1 - Ab) * (1 - Bb)) * 255);
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }

        internal void SoftLight()
        {
            for (int x = 0; x < form.imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < form.imgData1.img.GetLength(1); y++)
                {
                    PixelClassRGB firstImagePixel = form.imgData1.img[x, y];
                    PixelClassRGB secondImagePixel = form.imgData2.img[x, y];
                    double Ar = (double)firstImagePixel.R / 255;
                    double Ag = (double)firstImagePixel.G / 255;
                    double Ab = (double)firstImagePixel.B / 255;
                    double Br = (double)secondImagePixel.R / 255;
                    double Bg = (double)secondImagePixel.G / 255;
                    double Bb = (double)secondImagePixel.B / 255;

                    byte r = (byte)Math.Min(255, Br <= 0.5 ? (2 * Ar * Br) * 255 : (1 - 2 * (1 - Ar) * (1 - Br)) * 255);
                    byte g = (byte)Math.Min(255, Bg <= 0.5 ? (2 * Ag * Bg) * 255 : (1 - 2 * (1 - Ag) * (1 - Bg)) * 255);
                    byte b = (byte)Math.Min(255, Bb <= 0.5 ? (2 * Ab * Bb) * 255 : (1 - 2 * (1 - Ab) * (1 - Bb)) * 255);
                    form.imgData3.imgNew[x, y] = new PixelClassRGB(r, g, b);
                }
            }
        }
    }
}
