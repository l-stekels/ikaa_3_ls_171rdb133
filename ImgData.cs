﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms.DataVisualization.Charting;
using System.Collections.Generic;

namespace ikaa_8_ls_171rdb133
{
    public class ImgData
    {
        public PixelClassRGB[,] img;

        public PixelClassRGB[,] imgNew;

        public PixelClassHSV[,] imgHSV;

        public PixelClassHSV[,] imgHSVNew;

        public PixelClassCMYK[,] imgCMYK;

        public PixelClassYUV[,] imgYUV;

        public double[] histoRed;

        public double[] histoGreen;

        public double[] histoBlue;

        public double[] histoIntensity;

        public double[] histoHue;

        public double[] histoSaturation;

        public double[] histoValue;

        public ChartFormatter chartFormatter;

        public Chart chart;

        public int normalizerLevel = 0;

        public Threshold threshold;

        public int x0;
        public int y0;

        ~ImgData()
        {
            img = null;
            imgNew = null;
            imgHSV = null;
            imgHSVNew = null;
            imgCMYK = null;
            imgYUV = null;
            histoRed = null;
            histoGreen = null;
            histoBlue = null;
            histoIntensity = null;
            histoHue = null;
            histoSaturation = null;
            histoValue = null;
            chartFormatter = null;
            chart = null;
            threshold = null;
        }

        public void ReadImage(Bitmap bmp)
        {
            // nolasam datus no attēla
            img = new PixelClassRGB[bmp.Width, bmp.Height];
            imgNew = new PixelClassRGB[bmp.Width, bmp.Height];
            imgHSV = new PixelClassHSV[bmp.Width, bmp.Height];
            imgHSVNew = new PixelClassHSV[bmp.Width, bmp.Height];
            imgCMYK = new PixelClassCMYK[bmp.Width, bmp.Height];
            imgYUV = new PixelClassYUV[bmp.Width, bmp.Height];
            histoRed = new double[256];
            histoGreen = new double[256];
            histoBlue = new double[256];
            histoIntensity = new double[256];
            histoHue = new double[256];
            histoSaturation = new double[256];
            histoValue = new double[256];

            var bmpData = bmp.LockBits(
                new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.ReadOnly,
                bmp.PixelFormat
            );
            IntPtr ptr = IntPtr.Zero;
            int pixelComponents; // kanālu skaits

            // ja ir 24 bitu formāts
            if (bmpData.PixelFormat == PixelFormat.Format24bppRgb)
            {
                pixelComponents = 3;
                // ja ir 32 bitu formāts
            }
            else if (bmpData.PixelFormat == PixelFormat.Format32bppRgb)
            {
                pixelComponents = 4;
            }
            else
            {
                pixelComponents = 0;
            }

            var line = new byte[bmp.Width * pixelComponents];
            int xHalf = x0 = bmpData.Width / 2;
            int yHalf = y0 = bmpData.Height / 2;

            for (int y = 0; y < bmpData.Height; y++)
            {
                // nolasam atmiņā datus par attēlu
                // mēģinam nolasīt katru rindiņu.
                // stride - pikseļu rindas platums
                // nolasam no pirmā pikseļa
                ptr = bmpData.Scan0 + y * bmpData.Stride;
                Marshal.Copy(ptr, line, 0, line.Length);

                for (int x = 0; x < bmpData.Width; x++)
                {
                    byte r = line[pixelComponents * x + 2]; // red
                    byte g = line[pixelComponents * x + 1]; // green
                    byte b = line[pixelComponents * x]; // blue

                    img[x, y] = new PixelClassRGB(r, g, b, null, x - x0, y - y0);
                    imgNew[x, y] = img[x, y];
                    imgHSV[x, y] = new PixelClassHSV(r, g, b);
                    imgHSVNew[x, y] = imgHSV[x, y];
                    imgCMYK[x, y] = new PixelClassCMYK(r, g, b);
                    imgYUV[x, y] = new PixelClassYUV(r, g, b);
                    byte i = img[x, y].I;
                    byte h = (byte)Math.Round((double)imgHSV[x, y].H / 359 * 255);

                    histoRed[r]++;
                    histoGreen[g]++;
                    histoBlue[b]++;
                    histoIntensity[i]++;
                    histoHue[h]++;
                    histoSaturation[imgHSV[x, y].S]++;
                    histoValue[imgHSV[x, y].V]++;
                } // for x
            } // for y

            // nolasīšanas rezultāts
            bmp.UnlockBits(bmpData);
        }

        internal Image DrawTransformedImage()
        {
            if (img == null)
            {
                return null;
            }

            IntPtr ptr = IntPtr.Zero;
            int height = img.GetLength(1);
            int widght = img.GetLength(0);
            var bmp = new Bitmap(widght, height, PixelFormat.Format24bppRgb);

            var bmpData = bmp.LockBits(
                new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.WriteOnly,
                bmp.PixelFormat
                );

            var line = new byte[bmp.Width * 3]; // 3 kanāli

            for (int y = 0; y < bmpData.Height; y++)
            {
                for (int x = 0; x < bmpData.Width; x++)
                {
                    if ((img[x, y].X + x0) > 0 && (img[x, y].Y + y0) > 0 && (img[x, y].X + x0) < bmpData.Width && (img[x, y].Y + y0) < bmpData.Height)
                    {
                        line[3 * x] = img[img[x, y].X + x0, y0 + img[x, y].Y].B;
                        line[3 * x + 1] = img[img[x, y].X + x0, y0 + img[x, y].Y].G;
                        line[3 * x + 2] = img[img[x, y].X + x0, y0 + img[x, y].Y].R;
                    }
                    else
                    {
                        line[3 * x + 2] = line[3 * x + 1] = line[3 * x] = 0;
                    }
                } // for x
                ptr = bmpData.Scan0 + y * bmpData.Stride;
                Marshal.Copy(line, 0, ptr, line.Length);
            } // for y

            bmp.UnlockBits(bmpData);
            return bmp;
        }

        public Bitmap DrawImage(String mode = "RGB", Chart chart = null)
        {
            if (imgNew == null)
            {
                return null;
            }

            chartFormatter = new ChartFormatter(this, mode);
            if (chart != null)
            {
                this.chart = chart;
            }
            
            IntPtr ptr = IntPtr.Zero;
            int height = imgNew.GetLength(1);
            int widght = imgNew.GetLength(0);
            var bmp = new Bitmap(widght, height, PixelFormat.Format24bppRgb);

            var bmpData = bmp.LockBits(
                new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.WriteOnly,
                bmp.PixelFormat
                );

            var line = new byte[bmp.Width * 3]; // 3 kanāli

            for (int y = 0; y < bmpData.Height; y++)
            {
                for (int x = 0; x < bmpData.Width; x++)
                {
                    switch (mode)
                    {
                        case "RGB":
                            line[3 * x] = imgNew[x, y].B; // blue
                            line[3 * x + 1] = imgNew[x, y].G; // green
                            line[3 * x + 2] = imgNew[x, y].R; // red
                            break;
                        case "RED":
                            line[3 * x + 2] = imgNew[x, y].R; // red
                            line[3 * x + 1] = 0; // green
                            line[3 * x] = 0; // blue
                            break;
                        case "GREEN":
                            line[3 * x + 2] = 0; // red
                            line[3 * x + 1] = imgNew[x, y].G; // green
                            line[3 * x] = 0; // blue
                            break;
                        case "BLUE":
                            line[3 * x + 2] = 0; // red
                            line[3 * x + 1] = 0; // green
                            line[3 * x] = imgNew[x, y].B; // blue
                            break;
                        case "INTENSITY":
                            line[3 * x + 2] = imgNew[x, y].I;
                            line[3 * x + 1] = imgNew[x, y].I;
                            line[3 * x] = imgNew[x, y].I;
                            break;
                        case "INVERT":
                            line[3 * x + 2] = Convert.ToByte(255 - imgNew[x, y].R);
                            line[3 * x + 1] = Convert.ToByte(255 - imgNew[x, y].G);
                            line[3 * x] = Convert.ToByte(255 - imgNew[x, y].B);
                            break;
                        case "HSV":
                            line[3 * x + 2] = imgNew[x, y].HSVToRGB(imgHSV[x, y].H, imgHSV[x, y].S, imgHSV[x, y].V).R;
                            line[3 * x + 1] = imgNew[x, y].HSVToRGB(imgHSV[x, y].H, imgHSV[x, y].S, imgHSV[x, y].V).G;
                            line[3 * x] = imgNew[x, y].HSVToRGB(imgHSV[x, y].H, imgHSV[x, y].S, imgHSV[x, y].V).B;
                            break;
                        case "HUE":
                            line[3 * x + 2] = imgNew[x, y].HSVToRGB(imgHSV[x, y].H, 255, 255).R;
                            line[3 * x + 1] = imgNew[x, y].HSVToRGB(imgHSV[x, y].H, 255, 255).G;
                            line[3 * x] = imgNew[x, y].HSVToRGB(imgHSV[x, y].H, 255, 255).B;
                            break;
                        case "SATURATION":
                            line[3 * x + 2] = imgHSV[x, y].S;
                            line[3 * x + 1] = imgHSV[x, y].S;
                            line[3 * x] = imgHSV[x, y].S;
                            break;
                        case "VALUE":
                            line[3 * x + 2] = imgHSV[x, y].V;
                            line[3 * x + 1] = imgHSV[x, y].V;
                            line[3 * x] = imgHSV[x, y].V;
                            break;
                        case "CMYK":
                            line[3 * x + 2] = imgNew[x, y].CMYKToRGB(imgCMYK[x, y].C, imgCMYK[x, y].M, imgCMYK[x, y].Y, imgCMYK[x, y].K).R;
                            line[3 * x + 1] = imgNew[x, y].CMYKToRGB(imgCMYK[x, y].C, imgCMYK[x, y].M, imgCMYK[x, y].Y, imgCMYK[x, y].K).G;
                            line[3 * x] = imgNew[x, y].CMYKToRGB(imgCMYK[x, y].C, imgCMYK[x, y].M, imgCMYK[x, y].Y, imgCMYK[x, y].K).B;
                            break;
                        case "CYAN":
                            line[3 * x + 2] = imgNew[x, y].CMYKToRGB(imgCMYK[x, y].C, 0, 0, 0).R;
                            line[3 * x + 1] = imgNew[x, y].CMYKToRGB(imgCMYK[x, y].C, 0, 0, 0).G;
                            line[3 * x] = imgNew[x, y].CMYKToRGB(imgCMYK[x, y].C, 0, 0, 0).B;
                            break;
                        case "MAGENTA":
                            line[3 * x + 2] = imgNew[x, y].CMYKToRGB(0, imgCMYK[x, y].M, 0, 0).R;
                            line[3 * x + 1] = imgNew[x, y].CMYKToRGB(0, imgCMYK[x, y].M, 0, 0).G;
                            line[3 * x] = imgNew[x, y].CMYKToRGB(0, imgCMYK[x, y].M, 0, 0).B;
                            break;
                        case "YELLOW":
                            line[3 * x + 2] = imgNew[x, y].CMYKToRGB(0, 0, imgCMYK[x, y].Y, 0).R;
                            line[3 * x + 1] = imgNew[x, y].CMYKToRGB(0, 0, imgCMYK[x, y].Y, 0).G;
                            line[3 * x] = imgNew[x, y].CMYKToRGB(0, 0, imgCMYK[x, y].Y, 0).B;
                            break;
                        case "KEY":
                            line[3 * x + 2] = imgNew[x, y].CMYKToRGB(0, 0, 0, imgCMYK[x, y].K).R;
                            line[3 * x + 1] = imgNew[x, y].CMYKToRGB(0, 0, 0, imgCMYK[x, y].K).G;
                            line[3 * x] = imgNew[x, y].CMYKToRGB(0, 0, 0, imgCMYK[x, y].K).B;
                            break;
                        case "YUV":
                            line[3 * x + 2] = imgNew[x, y].YUVToRGB(imgYUV[x, y].Y, imgYUV[x, y].U, imgYUV[x, y].V).R;
                            line[3 * x + 1] = imgNew[x, y].YUVToRGB(imgYUV[x, y].Y, imgYUV[x, y].U, imgYUV[x, y].V).G;
                            line[3 * x] = imgNew[x, y].YUVToRGB(imgYUV[x, y].Y, imgYUV[x, y].U, imgYUV[x, y].V).B;
                            break;
                        case "Y":
                            line[3 * x + 2] = imgNew[x, y].YUVToRGB(imgYUV[x, y].Y, 128, 128).R;
                            line[3 * x + 1] = imgNew[x, y].YUVToRGB(imgYUV[x, y].Y, 128, 128).G;
                            line[3 * x] = imgNew[x, y].YUVToRGB(imgYUV[x, y].Y, 128, 128).B;
                            break;
                        case "U":
                            line[3 * x + 2] = imgNew[x, y].YUVToRGB(128, imgYUV[x, y].U, 128).R;
                            line[3 * x + 1] = imgNew[x, y].YUVToRGB(128, imgYUV[x, y].U, 128).G;
                            line[3 * x] = imgNew[x, y].YUVToRGB(128, imgYUV[x, y].U, 128).B;
                            break;
                        case "V":
                            line[3 * x + 2] = imgNew[x, y].YUVToRGB(128, 128, imgYUV[x, y].V).R;
                            line[3 * x + 1] = imgNew[x, y].YUVToRGB(128, 128, imgYUV[x, y].V).G;
                            line[3 * x] = imgNew[x, y].YUVToRGB(128, 128, imgYUV[x, y].V).B;
                            break;
                    }
                } // for x
                ptr = bmpData.Scan0 + y * bmpData.Stride;
                Marshal.Copy(line, 0, ptr, line.Length);
            } // for y

            if (chart != null)
            {
                chartFormatter.DrawChart();
            }
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        public int CalculateRangeStart(double[] histogram)
        {
            int start = 0;
            double highest = 0;
            if (normalizerLevel > 0)
            {
                for (int i = 0; i < 256; i++)
                {
                    if (histogram[i] > highest)
                    {
                        highest = histogram[i];
                    }
                }
            }
            int lowest = highest > 0 ? (int)(highest * normalizerLevel / 100) : 0;
            for (int i = 0; i < 256; i++)
            {
                if (histogram[i] > lowest)
                {
                    start = i;
                    break;
                }
            }
            return start;
        }

        public int CalculateRangeEnd(double[] histogram)
        {
            int end = 255;
            double highest = 0;
            if (normalizerLevel > 0)
            {
                for (int i = 0; i < 256; i++)
                {
                    if (histogram[i] > highest)
                    {
                        highest = histogram[i];
                    }
                }
            }
            int lowest = highest > 0 ? (int)(highest * normalizerLevel / 100) : 0;
            for (int i = 255; i >= 0; i--)
            {
                if (histogram[i] > lowest)
                {
                    end = i;
                    break;
                }
            }
            return end;
        }

        private double CalculateK(double[] histogram)
        {
            return 255 / (double) (CalculateRangeEnd(histogram) - CalculateRangeStart(histogram));
        }

        public double[] stretch(double[] histogram)
        {
            int rangeStart = CalculateRangeStart(histogram);
            int rangeEnd = CalculateRangeEnd(histogram);
            double k = CalculateK(histogram);
            double[] newHistogram = new double[256];

            for (int i = 0; i < 256; i++)
            {
                if (i < rangeStart | i > rangeEnd)
                {
                    continue;
                }
                int j = (int)Math.Round(k * (i - rangeStart));
                newHistogram[j] = histogram[i];
            }
            return newHistogram;
        }

        public Bitmap histogramStretch(string mode, Chart chart, int normalize = 0)
        {
            if (imgNew == null)
            {
                return null;
            }
            normalizerLevel = normalize;
            chartFormatter = new ChartFormatter(this, mode);
            this.chart = chart;

            IntPtr ptr = IntPtr.Zero;
            int height = img.GetLength(1);
            int widght = img.GetLength(0);
            var bmp = new Bitmap(widght, height, PixelFormat.Format24bppRgb);

            var bmpData = bmp.LockBits(
                new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.WriteOnly,
                bmp.PixelFormat
                );

            var line = new byte[bmp.Width * 3]; // 3 kanāli

            double rangeStartRed = CalculateRangeStart(histoRed);
            double rangeStartGreen = CalculateRangeStart(histoGreen);
            double rangeStartBlue = CalculateRangeStart(histoBlue);
            double rangeStartIntensity = CalculateRangeStart(histoIntensity);
            double rangeStartSaturation = CalculateRangeStart(histoSaturation);
            double rangeStartValue = CalculateRangeStart(histoValue);
            double redK = CalculateK(histoRed);
            double greenK = CalculateK(histoGreen);
            double blueK = CalculateK(histoBlue);
            double intensityK = CalculateK(histoIntensity);
            double saturationK = CalculateK(histoSaturation);
            double valueK = CalculateK(histoValue);

            for (int y = 0; y < bmpData.Height; y++)
            {
                for (int x = 0; x < bmpData.Width; x++)
                {
                    byte r = 0, g = 0, b = 0;
                    switch (mode)
                    {
                        case "RED":
                            r = imgNew[x, y].R;
                            if (r > 0)
                            {
                                r = (byte)Math.Round(redK * (r - rangeStartRed));
                            }
                            break;
                        case "GREEN":
                            g = imgNew[x, y].G;
                            if (g > 0)
                            {
                                g = (byte)Math.Round(greenK * (g - rangeStartGreen));
                            }
                            break;
                        case "BLUE":
                            b = imgNew[x, y].B;
                            if (b > 0)
                            {
                                b = (byte)Math.Round(blueK * (b - rangeStartBlue));
                            }
                            break;
                        case "INTENSITY":
                            byte i = imgNew[x, y].I;
                            r = g = b = i;
                            if (i > 0)
                            {
                                r = g = b = (byte)Math.Round(intensityK * (i - rangeStartIntensity));
                            }
                            break;
                        case "HSV":
                            {
                                int h = imgHSVNew[x, y].H;
                                byte s = imgHSVNew[x, y].S;
                                byte v = imgHSVNew[x, y].V;
                                if (s > 0)
                                {
                                    s = (byte)Math.Round(saturationK * (s - rangeStartSaturation));
                                }
                                if (v > 0)
                                {
                                    v = (byte)Math.Round(valueK * (v - rangeStartValue));
                                }
                                r = imgNew[x, y].HSVToRGB(h, s, v).R;
                                g = imgNew[x, y].HSVToRGB(h, s, v).G;
                                b = imgNew[x, y].HSVToRGB(h, s, v).B;
                                break;
                            }
                        case "HUE":
                            r = imgNew[x, y].HSVToRGB(imgHSVNew[x, y].H, 255, 255).R;
                            g = imgNew[x, y].HSVToRGB(imgHSVNew[x, y].H, 255, 255).G;
                            b = imgNew[x, y].HSVToRGB(imgHSVNew[x, y].H, 255, 255).B;
                            break;
                        case "SATURATION":
                            {
                                byte s = imgHSVNew[x, y].S;
                                if (s > 0)
                                {
                                    s = (byte)Math.Round(saturationK * (s - rangeStartSaturation));
                                }
                                r = g = b = s;
                                break;
                            }
                        case "VALUE":
                            {
                                byte v = imgHSVNew[x, y].V;
                                if (v > 0)
                                {
                                    v = (byte)Math.Round(valueK * (v - rangeStartValue));
                                }
                                r = g = b = v;
                                break;
                            }
                        default:
                            r = imgNew[x, y].R;
                            g = imgNew[x, y].G;
                            b = imgNew[x, y].B;
                            if (r > 0)
                            {
                                r = (byte)Math.Round(redK * (r - rangeStartRed));
                            }
                            if (g > 0)
                            {
                                g = (byte)Math.Round(greenK * (g - rangeStartGreen));
                            }
                            if (b > 0)
                            {
                                b = (byte)Math.Round(blueK * (b - rangeStartBlue));
                            }
                            break;
                    }
                    line[3 * x] = b; // blue
                    line[3 * x + 1] = g; // green
                    line[3 * x + 2] = r; // red
                } // for x
                ptr = bmpData.Scan0 + y * bmpData.Stride;
                Marshal.Copy(line, 0, ptr, line.Length);
            } // for y
            chartFormatter.DrawChart();
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        public void ApplyEdgeSegmentation(EdgeDetection edgeDetection)
        {
            if (img == null)
            {
                return;
            }
            int margin = 1;
            if (edgeDetection.size == 2)
            {
                margin = 0;
            }
            for (int x = 1; x < img.GetLength(0) - margin; x++)
            {
                for (int y = 1; y < img.GetLength(1) - margin; y++)
                {
                    int gXR = 0, gYR = 0, gXG = 0, gYG = 0, gXB = 0, gYB = 0, gXI = 0, gYI = 0;

                    for (int fI = 0; fI < edgeDetection.size; fI++)
                    {
                        for (int fJ = 0; fJ < edgeDetection.size; fJ++)
                        {
                            PixelClassRGB pixel = img[x + fI - 1, y + fJ - 1];
                            int edgeGX = edgeDetection.gX[fI, fJ];
                            int edgeGY = edgeDetection.gY[fI, fJ];
                            gXR += pixel.R * edgeGX;
                            gYR += pixel.R * edgeGY;
                            gXG += pixel.G * edgeGX;
                            gYG += pixel.G * edgeGY;
                            gXB += pixel.B * edgeGX;
                            gYB += pixel.B * edgeGY;
                            gXI += pixel.I * edgeGX;
                            gYI += pixel.I * edgeGY;
                        }
                    }
                    imgNew[x, y] = new PixelClassRGB(
                        (byte)(Convert.ToInt32(Math.Sqrt(gXR * gXR + gYR * gYR)) < 128 ? 0 : 255),
                        (byte)(Convert.ToInt32(Math.Sqrt(gXG * gXG + gYG * gYG)) < 128 ? 0 : 255),
                        (byte)(Convert.ToInt32(Math.Sqrt(gXB * gXB + gYB * gYB)) < 128 ? 0 : 255),
                        (byte)(Convert.ToInt32(Math.Sqrt(gXI * gXI + gYI * gYI)) < 128 ? 0 : 255));
                }
            }
        }

        private void CalculateSegmentation(int tLow, int tHigh, string mode = "INTENSITY", bool vertical = true)
        {
            Threshold threshold = new Threshold(this, mode);

            for (int x = 1; x < img.GetLength(0) - 1; x++)
            {
                for (int y = 1; y < img.GetLength(1) - 1; y++)
                {
                    byte color = img[x, y].I;

                    color = mode == "RED" ? img[x, y].R : color;
                    color = mode == "GREEN" ? img[x, y].G : color;
                    color = mode == "BLUE" ? img[x, y].B : color;
                    byte value;

                    if (vertical)
                    {
                        value = (byte)(color <= tLow ? 0 : color > tHigh ? 255 : 128);
                    } else
                    {

                        value = (byte)(255 - (threshold.histogram[color] < tLow ? 255 : 0));
                    }

                    imgNew[x, y] = new PixelClassRGB(value);
                }
            }
        }

        public Bitmap FilterImageData()
        {
            if (img == null)
            {
                return null;
            }
            IntPtr ptr = IntPtr.Zero;
            int Height = img.GetLength(1);
            int Width = img.GetLength(0);
            var bmp = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                    ImageLockMode.WriteOnly, bmp.PixelFormat);
            var line = new byte[bmp.Width * 3];

            for (int y = 0; y < bmpData.Height; y++)
            {
                for (int x = 0; x < bmpData.Width; x++)
                {
                    line[3 * x] = imgNew[x, y].B;
                    line[3 * x + 1] = imgNew[x, y].G;
                    line[3 * x + 2] = imgNew[x, y].R;
                }
                ptr = bmpData.Scan0 + y * bmpData.Stride;
                Marshal.Copy(line, 0, ptr, line.Length);
            }

            bmp.UnlockBits(bmpData);
            return bmp;
        }

        public void FilterImage(Filter filter)
        {
            if (img == null)
            {
                return;
            }
            int margin = (filter.size - 1) / 2;
            int middle = (filter.size * filter.size - 1) / 2;
            for (int x = margin; x < img.GetLength(0) - margin; x++)
            {
                for (int y = margin; y < img.GetLength(1) - margin; y++)
                {
                    double r = 0;
                    double g = 0;
                    double b = 0;
                    List<byte> valuesList = new List<byte>();

                    for (int fi = -margin; fi <= margin; fi++)
                    {
                        for (int fj = -margin; fj <= margin; fj++)
                        {
                            if (filter.median)
                            {
                                valuesList.Add(img[x + fi, y + fj].I);
                            }
                            else
                            {
                                r += (double)img[x + fi, y + fj].R * filter.filter[fi + margin, fj + margin];
                                g += (double)img[x + fi, y + fj].G * filter.filter[fi + margin, fj + margin];
                                b += (double)img[x + fi, y + fj].B * filter.filter[fi + margin, fj + margin];
                            }
                        }
                    }

                    if (filter.median)
                    {
                        byte[] values = valuesList.ToArray();
                        Array.Sort(values);
                        Array.Reverse(values);
                        byte color = values[middle];
                        imgNew[x, y] = new PixelClassRGB(color, color, color);
                    }
                    else
                    {
                        imgNew[x, y] = new PixelClassRGB(
                            (byte)Math.Max(0, Math.Min(255, r / filter.K)),
                            (byte)Math.Max(0, Math.Min(255, g / filter.K)),
                            (byte)Math.Max(0, Math.Min(255, b / filter.K))
                        );
                    }
                }
            }
        }

        public int HistogramVerticalAdaptiveSegmentation(string mode)
        {
           if (img == null)
            {
                return 0;
            }
            int tLow = (new Threshold(this, mode)).Tlow();

            CalculateSegmentation(tLow, 0, mode);

            return tLow;
        }

        public int[] HistogramVerticalManualSegmentation(string mode, int value, int value2 = 0)
        {
            int[] result = { value, value2 };
            if (img != null)
            {
                CalculateSegmentation(value, value2, mode);
            }

            return result;
        }

        public int HistogramHorizontalManualSegmentation(string mode, int value)
        {
            CalculateSegmentation(value, 0, mode, false);
            return value;
        }
    }
}
