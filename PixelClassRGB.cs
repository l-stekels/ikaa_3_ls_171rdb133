﻿using System;
using System.Xml;

namespace ikaa_8_ls_171rdb133
{
    public class PixelClassRGB
    {
        public byte R; // Red
        public byte G; // Green
        public byte B; // Blue
        public byte I; // Intensity
        public int X;
        public int Y;

        public PixelClassRGB()
        {
            R = 0;
            G = 0;
            B = 0;
            I = 0;
        }

        public PixelClassRGB(byte r, byte g, byte b, Nullable<byte> i = null, int x = 0, int y = 0)
        {
            R = r;
            G = g;
            B = b;
            if (i == null)
            {
                i = (byte)Math.Round(0.0722f * b + 0.7152f * g + 0.2126f * r);
            }
            I = (byte)i;
            X = x;
            Y = y;
        }

        public PixelClassRGB(byte value)
        {
            R = value;
            G = value;
            B = value;
            I = value;
        }

        public PixelClassRGB HSVToRGB(int h, byte s, byte v)
        {
            byte r = 0;
            byte g = 0;
            byte b = 0;

            int Hi = Convert.ToInt32(h / 60);
            byte vMin = Convert.ToByte((255 - s) * v / 255);
            int a = Convert.ToInt32((v - vMin) * (h % 60) / 60);

            byte vInc = Convert.ToByte(vMin + a);
            byte Vdec = Convert.ToByte(v - a);

            switch (Hi)
            {
                case 0:
                    r = v;
                    g = vInc;
                    b = vMin;
                    break;
                case 1:
                    r = Vdec;
                    g = v;
                    b = vMin;
                    break;
                case 2:
                    r = vMin;
                    g = v;
                    b = vInc;
                    break;
                case 3:
                    r = vMin;
                    g = Vdec;
                    b = v;
                    break;
                case 4:
                    r = vInc;
                    g = vMin;
                    b = v;
                    break;
                case 5:
                    r = v;
                    g = vMin;
                    b = Vdec;
                    break;
            }
            return new PixelClassRGB(r, g, b);
        }

        public PixelClassRGB CMYKToRGB(double C, double M, double Y, double K)
        {
            return new PixelClassRGB(
                (byte)(255 * (1 - C) * (1 - K)),
                (byte)(255 * (1 - M) * (1 - K)),
                (byte)(255 * (1 - Y) * (1 - K))
            );
        }

        public PixelClassRGB YUVToRGB(double Y, double U, double V)
        {
            return new PixelClassRGB(
                (byte)(Y + 1.13983 * (V - 128)),
                (byte)(Y - 0.39465 * (U - 128) - 0.58060 * (V - 128)),
                (byte)(Y + 2.03211 * (U - 128))
            );
        }
    }
}
