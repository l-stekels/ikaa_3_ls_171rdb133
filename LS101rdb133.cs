﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Drawing.Imaging; // nolasam piksela formatu

namespace ikaa_8_ls_171rdb133
{
    public partial class LS_171rdb133 : Form
    {
        public ImgData imgData = new ImgData();

        public ColorDialog colorDialog = new ColorDialog();

        public string lastChannel = "RGB";

        public bool verticalSegmentation = true;

        public LS_171rdb133()
        {
            InitializeComponent();
            colorDialog.AllowFullOpen = false;
            colorDialog.ShowHelp = true;
            colorDialog.Color = Color.FromArgb(255, 255, 255);
        }

        private void OpenFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void SaveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                double x = e.X;
                double y = e.Y;

                double k;
                if (pictureBox1.Image.Width > pictureBox1.Image.Height)
                {
                    k = (double)pictureBox1.Image.Width / pictureBox1.Width;
                }
                else
                {
                    k = (double)pictureBox1.Image.Height / pictureBox1.Height;
                }
                double nobideX = (pictureBox1.Width * k - pictureBox1.Image.Width) / 2;
                double nobideY = (pictureBox1.Height * k - pictureBox1.Image.Height) / 2;
                x = Math.Round(e.X * k - nobideX);
                y = Math.Round(e.Y * k - nobideY);

                xPosition.Text = Convert.ToString(x);
                yPosition.Text = Convert.ToString(y);

                if (x < 0 | x >= pictureBox1.Image.Width | y < 0 | y >= pictureBox1.Image.Height) return;

                // RGB
                PixelClassRGB rgb = imgData.img[(int)x, (int)y];
                redValue.Text = Convert.ToString(rgb.R);
                greenValue.Text = Convert.ToString(rgb.G);
                blueValue.Text = Convert.ToString(rgb.B);
                // HSV
                PixelClassHSV hsv = imgData.imgHSV[(int)x, (int)y];
                hueValue.Text = Convert.ToString(hsv.H);
                saturationValue.Text = string.Concat(Convert.ToString(Math.Round((double)hsv.S / 255 * 100)), "%");
                valueValue.Text = string.Concat(Convert.ToString(Math.Round((double)hsv.V / 255 * 100)), "%");
                // CMYK
                PixelClassCMYK cmyk = imgData.imgCMYK[(int)x, (int)y];
                cyanValue.Text = string.Concat(Convert.ToString(Math.Round(cmyk.C * 100)), "%");
                magentaValue.Text = string.Concat(Convert.ToString(Math.Round(cmyk.M * 100)), "%");
                yellowValue.Text = string.Concat(Convert.ToString(Math.Round(cmyk.Y * 100)), "%");
                keyValue.Text = string.Concat(Convert.ToString(Math.Round(cmyk.K * 100)), "%");
                // YUV
                PixelClassYUV yuv = imgData.imgYUV[(int)x, (int)y];
                yValue.Text = Convert.ToString(Math.Round(yuv.Y));
                uValue.Text = Convert.ToString(Math.Round(yuv.U));
                vValue.Text = Convert.ToString(Math.Round(yuv.V));
                return;
            }

            xPosition.Text = Convert.ToString(e.X);
            yPosition.Text = Convert.ToString(e.Y);
        }

        private void PictureBox2_MouseClick(object sender, MouseEventArgs e)
        {
            if (pictureBox2.Image != null)
            {
                if (pictureBox2.Width > pictureBox2.Image.Width | pictureBox2.Height > pictureBox2.Image.Height)
                {
                    double k;
                    if (pictureBox2.Image.Width > pictureBox2.Image.Height)
                    {
                        k = (double)pictureBox2.Image.Width / pictureBox2.Width;
                    }
                    else
                    {
                        k = (double)pictureBox2.Image.Height / pictureBox2.Height;
                    }
                    double nobideX = (pictureBox2.Width * k - pictureBox2.Image.Width) / 2;
                    double nobideY = (pictureBox2.Height * k - pictureBox2.Image.Height) / 2;
                    double kx = Math.Round(e.X * k - nobideX);
                    double ky = Math.Round(e.Y * k - nobideY);

                    ((Bitmap)pictureBox2.Image).SetPixel((int)kx, (int)ky, colorDialog.Color);
                    pictureBox2.Refresh();
                }
            }
        }

        private void CopyImageMode(string mode)
        {
            lastChannel = mode;
            pictureBox2.Image = imgData.DrawImage(mode, histogramNew);
        }

        private void RadioRGB_CheckedChanged(object sender, EventArgs e)
        {
            compositeColors.Checked = true;
            colorChannel1.Text = "Red";
            colorChannel2.Text = "Green";
            colorChannel3.Text = "Blue";
            colorChannel4.Text = "Intensity";
            colorChannel4.Visible = true;

            if (pictureBox1.Image != null)
            {
                CopyImageMode("RGB");
            }
        }

        private void RadioHSV_CheckedChanged(object sender, EventArgs e)
        {
            compositeColors.Checked = true;
            colorChannel1.Text = "Hue";
            colorChannel2.Text = "Saturation";
            colorChannel3.Text = "Value";
            colorChannel4.Visible = false;

            if (pictureBox1.Image != null)
            {
                CopyImageMode("HSV");
            }
        }

        private void RadioCMYK_CheckedChanged(object sender, EventArgs e)
        {
            compositeColors.Checked = true;
            colorChannel1.Text = "Cyan";
            colorChannel2.Text = "Magenta";
            colorChannel3.Text = "Yellow";
            colorChannel4.Text = "Black";
            colorChannel4.Visible = true;

            if (pictureBox1.Image != null)
            {
                CopyImageMode("CMYK");
            }
        }

        private void RadioYUV_CheckedChanged(object sender, EventArgs e)
        {
            compositeColors.Checked = true;
            colorChannel1.Text = "Y";
            colorChannel2.Text = "U";
            colorChannel3.Text = "V";
            colorChannel4.Visible = false;

            if (pictureBox1.Image != null)
            {
                CopyImageMode("YUV");
            }
        }

        private string CurrentColorScheme()
        {
            if (radioHSV.Checked)
            {
                return "HSV";
            }

            if (radioCMYK.Checked)
            {
                return "CMYK";
            }

            if (radioYUV.Checked)
            {
                return "YUV";
            }
            return "RGB";
        }

        private void CompositeColors_CheckedChanged(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                CopyImageMode(CurrentColorScheme());
            }
        }

        private void ColorChannel1_CheckedChanged(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
            {
                return;
            }
            if (CurrentColorScheme() == "HSV")
            {
                CopyImageMode("HUE");
                return;
            }
            if (CurrentColorScheme() == "CMYK")
            {
                CopyImageMode("CYAN");
                return;
            }
            if (CurrentColorScheme() == "YUV")
            {
                CopyImageMode("Y");
                return;
            }
            CopyImageMode("RED");
            SetTrackbar1Maximum("RED");
        }

        private void ColorChannel2_CheckedChanged(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
            {
                return;
            }
            if (CurrentColorScheme() == "HSV")
            {
                CopyImageMode("SATURATION");
                return;
            }
            if (CurrentColorScheme() == "CMYK")
            {
                CopyImageMode("MAGENTA");
                return;
            }
            if (CurrentColorScheme() == "YUV")
            {
                CopyImageMode("U");
                return;
            }
            CopyImageMode("GREEN");
            SetTrackbar1Maximum("GREEN");
        }

        private void ColorChannel3_CheckedChanged(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
            {
                return;
            }
            if (CurrentColorScheme() == "HSV")
            {
                CopyImageMode("VALUE");
                return;
            }
            if (CurrentColorScheme() == "CMYK")
            {
                CopyImageMode("YELLOW");
                return;
            }
            if (CurrentColorScheme() == "YUV")
            {
                CopyImageMode("V");
                return;
            }
            CopyImageMode("BLUE");
            SetTrackbar1Maximum("BLUE");
        }

        private void ColorChannel4_CheckedChanged(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
            {
                return;
            }
            if (CurrentColorScheme() == "CMYK")
            {
                CopyImageMode("KEY");
                return;
            }
            CopyImageMode("INTENSITY");
            SetTrackbar1Maximum("INTENSITY");
        }

        private void loadImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Bitmap.FromFile(openFileDialog1.FileName);
            }
            Bitmap bmp = (Bitmap)pictureBox1.Image.Clone();
            imgData.ReadImage(bmp);
            pictureBox2.Image = imgData.DrawImage("RGB", histogramOriginal);
            imgData.chartFormatter.DrawChart(histogramNew);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Jpeg(*.jpg)|*.jpg";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox2.Image.Save(saveFileDialog1.FileName, ImageFormat.Jpeg);
            }
        }

        private void invertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imgData.img != null)
            {
                CopyImageMode("INVERT");
            }
        }

        private void selectColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorDialog.ShowDialog();
            button1.BackColor = colorDialog.Color;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            colorDialog.ShowDialog();
            button1.BackColor = colorDialog.Color;
        }

        private void normalizeValue_Scroll(object sender, EventArgs e)
        {
            normalizeValueLabel.Text = string.Concat(Convert.ToString(normalizeValue.Value), " %");
        }

        private void stretch_CheckedChanged(object sender, EventArgs e)
        {
            normalizeValue.Visible = false;
            normalizeLabel.Visible = false;
            normalizeValueLabel.Visible = false;
        }

        private void normalize_CheckedChanged(object sender, EventArgs e)
        {
            normalizeValue.Visible = true;
            normalizeLabel.Visible = true;
            normalizeValueLabel.Visible = true;
            normalizeValueLabel.Text = string.Concat(Convert.ToString(normalizeValue.Value), " %");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (stretch.Checked)
            {
                pictureBox2.Image = imgData.histogramStretch(lastChannel, histogramNew);
                return;
            }
            pictureBox2.Image = imgData.histogramStretch(lastChannel, histogramNew, normalizeValue.Value);
        }

        private void ApplyFilter(Filter filter)
        {
            imgData.FilterImage(filter);
            radioRGB.Checked = true;
            compositeColors.Checked = true;
            pictureBox2.Image = imgData.FilterImageData();
            GC.Collect();
        }

        private void blur19ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imgData.img == null)
            {
                return;
            }
            Filter filter = new Filter();
            ApplyFilter(filter.Blur3x3(9));
        }

        private void blur110ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imgData.img == null)
            {
                return;
            }
            Filter filter = new Filter();
            ApplyFilter(filter.Blur3x3(10));
        }

        private void blur116ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imgData.img == null)
            {
                return;
            }
            Filter filter = new Filter();
            ApplyFilter(filter.Blur3x3(16));
        }

        private void median3x3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imgData.img == null)
            {
                return;
            }
            ApplyFilter(new Filter(3, true));
        }

        private void median5x5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imgData.img == null)
            {
                return;
            }
            ApplyFilter(new Filter(5, true));
        }

        private void median7x7ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imgData.img == null)
            {
                return;
            }
            ApplyFilter(new Filter(7, true));
        }

        private void sharpen1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imgData.img == null)
            {
                return;
            }
            Filter filter = new Filter();
            ApplyFilter(filter.Sharpen3x3("-1 5 -1"));
        }

        private void sharpen2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imgData.img == null)
            {
                return;
            }
            Filter filter = new Filter();
            ApplyFilter(filter.Sharpen3x3("-1 9 -1"));
        }

        private void sharpen3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (imgData.img == null)
            {
                return;
            }
            Filter filter = new Filter();
            ApplyFilter(filter.Sharpen3x3("-2 9 -2"));
        }

        private void ApplyEdgeDetection(EdgeDetection edgeDetection, string mode = "RGB")
        {
            imgData.ApplyEdgeSegmentation(edgeDetection);
            pictureBox2.Image = imgData.DrawImage(mode, histogramNew);
            GC.Collect();
        }

        private void rGBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            compositeColors.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("sobel"));
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel1.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("sobel"), "RED");
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel2.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("sobel"), "GREEN");
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel3.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("sobel"), "BLUE");
        }

        private void intensityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel4.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("sobel"), "INTENSITY");
        }

        private void rGBToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            compositeColors.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("prewitt"));
        }

        private void redToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel1.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("prewitt"), "RED");
        }

        private void greenToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel2.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("prewitt"), "GREEN");
        }

        private void blueToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel3.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("prewitt"), "BLUE");
        }

        private void intensityToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel4.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("prewitt"), "INTENSITY");
        }

        private void rGBToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            compositeColors.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("roberts"));
        }

        private void redToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel1.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("roberts"), "RED");
        }

        private void greenToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel2.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("roberts"), "GREEN");
        }

        private void blueToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel3.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("roberts"), "BLUE");
        }

        private void intensityToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            radioRGB.Checked = true;
            colorChannel4.Checked = true;
            ApplyEdgeDetection(new EdgeDetection("roberts"), "INTENSITY");
        }

        private void ResetTrackbars()
        {
            thresholdTrackbar1.Visible = false;
            thresholdTrackbar1.Value = 0;
            thresholdTrackbar1.Minimum = 0;
            thresholdTrackbar1.Maximum = 255;
            thresholdTrackbar2.Value = 0;
            thresholdTrackbar2.Minimum = 0;
            thresholdTrackbar2.Maximum = 255;
            thresholdTrackbar2.Visible = false;
        }

        private void VerticalAdaptiveThresholdClick(string mode)
        {
            verticalSegmentation = true;
            radioRGB.Checked = true;
            EnableColorChannel(mode);
            threshold1Label.Text = "Vertical Adaptive Threshold:";
            threshold1Label.Visible = true;
            thresholdTrackbar1.Visible = false;
            thresholdTrackbar2.Visible = false;
            threshold2Label.Visible = false;
            threshold2Value.Visible = false;
            if (imgData.img == null)
            {
                return;
            }
            threshold1Value.Text = Convert.ToString(imgData.HistogramVerticalAdaptiveSegmentation(mode));
            threshold1Value.Visible = true;
            pictureBox2.Image = imgData.DrawImage(mode, histogramNew);
        }

        private void redVerticalAdaptiveThreshold_Click(object sender, EventArgs e)
        {
            VerticalAdaptiveThresholdClick("RED");
        }

        private void greenVerticalAdaptiveThreshold_Click(object sender, EventArgs e)
        {
            VerticalAdaptiveThresholdClick("GREEN");
        }

        private void blueVerticalAdaptiveThreshold_Click(object sender, EventArgs e)
        {
            VerticalAdaptiveThresholdClick("BLUE");
        }

        private void intensityVerticalAdaptiveThreshold_Click(object sender, EventArgs e)
        {
            VerticalAdaptiveThresholdClick("INTENSITY");
        }

        private void thresholdTrackbar1_Scroll(object sender, EventArgs e)
        {
            string mode = CurrentColorChanel();
            string value = "";
            if (verticalSegmentation)
            {
                value = Convert.ToString(imgData.HistogramVerticalManualSegmentation(mode, thresholdTrackbar1.Value, thresholdTrackbar2.Value)[0]);
            } 
            if (!verticalSegmentation)
            {
                value = Convert.ToString(imgData.HistogramHorizontalManualSegmentation(mode, thresholdTrackbar1.Value));
            }
            threshold1Value.Text = value;
            pictureBox2.Image = imgData.DrawImage(mode, histogramNew);
        }

        private string CurrentColorChanel()
        {
            if (colorChannel1.Checked)
            {
                return "RED";
            }
            if (colorChannel2.Checked)
            {
                return "GREEN";
            }
            if (colorChannel3.Checked)
            {
                return "BLUE";
            }
            return "INTENSITY";
        }

        private void EnableColorChannel(string mode)
        {
            if (mode == "RED")
            {
                colorChannel1.Checked = true;
            }
            if (mode == "GREEN")
            {
                colorChannel2.Checked = true;
            }
            if (mode == "BLUE")
            {
                colorChannel3.Checked = true;
            }
            if (mode == "INTENSITY")
            {
                colorChannel4.Checked = true;
            }
        }

        private void VerticalManualThresholdClick(string mode, bool two = false)
        {
            verticalSegmentation = true;
            radioRGB.Checked = true;
            EnableColorChannel(mode);
            threshold1Label.Text = "Vertical Manual Threshold:";
            threshold1Label.Visible = true;
            ResetTrackbars();
            thresholdTrackbar1.Visible = true;
            if (imgData.img == null)
            {
                return;
            }
            int[] segmentationResults = imgData.HistogramVerticalManualSegmentation(mode, 0, 255);
            threshold1Value.Text = Convert.ToString(segmentationResults[0]);
            threshold1Value.Visible = true;
            if (two)
            {
                threshold2Label.Text = "Vertical Manual Threshold 2:";
                threshold2Label.Visible = true;
                thresholdTrackbar2.Visible = true;
                threshold2Value.Text = Convert.ToString(segmentationResults[1]);
                thresholdTrackbar2.Value = segmentationResults[1];
                threshold2Value.Visible = true;
            }
            pictureBox2.Image = imgData.DrawImage(mode, histogramNew);
        }

        private void redVerticalManualThreshold_Click(object sender, EventArgs e)
        {
            VerticalManualThresholdClick("RED");
        }

        private void greenVerticalManualThreshold_Click(object sender, EventArgs e)
        {
            VerticalManualThresholdClick("GREEN");
        }

        private void blueVerticalManualThreshold_Click(object sender, EventArgs e)
        {
            VerticalManualThresholdClick("BLUE");
        }

        private void intensityVerticalManualThreshold_Click(object sender, EventArgs e)
        {
            VerticalManualThresholdClick("INTENSITY");
        }

        private void SetTrackbar1Maximum(string mode)
        {
            if (verticalSegmentation)
            {
                return;
            }
            switch (mode)
            {
                case "RED":
                    thresholdTrackbar1.Maximum = imgData.histoRed != null ? (int)imgData.histoRed.Max() : 0;
                    break;
                case "GREEN":
                    thresholdTrackbar1.Maximum = imgData.histoGreen != null ? (int)imgData.histoGreen.Max() : 0;
                    break;
                case "BLUE":
                    thresholdTrackbar1.Maximum = imgData.histoBlue != null ? (int)imgData.histoBlue.Max() : 0;
                    break;
                default:
                    thresholdTrackbar1.Maximum = imgData.histoIntensity != null ? (int)imgData.histoIntensity.Max() : 0;
                    break;
            }
        }

        private void HorizontalManualThresholdClick(string mode)
        {
            verticalSegmentation = false;
            radioRGB.Checked = true;
            EnableColorChannel(mode);
            threshold1Label.Text = "Horizontal Manual Threshold:";
            threshold1Label.Visible = true;
            ResetTrackbars();
            thresholdTrackbar1.Visible = true;
            thresholdTrackbar1.Value = 0;
            thresholdTrackbar1.Minimum = 0;
            SetTrackbar1Maximum(mode);
            if (imgData.img == null)
            {
                return;
            }
            threshold1Value.Text = Convert.ToString(imgData.HistogramHorizontalManualSegmentation(mode, 0));
            threshold1Value.Visible = true;
            pictureBox2.Image = imgData.DrawImage(mode, histogramNew);
        }

        private void redHorizontalManualThreshold_Click(object sender, EventArgs e)
        {
            HorizontalManualThresholdClick("RED");
        }

        private void greenHorizontalManualThreshold_Click(object sender, EventArgs e)
        {
            HorizontalManualThresholdClick("GREEN");
        }

        private void blueHorizontalManualThreshold_Click(object sender, EventArgs e)
        {
            HorizontalManualThresholdClick("BLUE");
        }

        private void intensityHorizontalManualThreshold_Click(object sender, EventArgs e)
        {
            HorizontalManualThresholdClick("INTENSITY");
        }

        private void redTwoVerticalManualThresholds_Click(object sender, EventArgs e)
        {
            VerticalManualThresholdClick("RED", true);
        }

        private void thresholdTrackbar2_Scroll(object sender, EventArgs e)
        {
            string mode = CurrentColorChanel();
            if (thresholdTrackbar2.Value < thresholdTrackbar1.Value)
            {
                thresholdTrackbar1.Value = thresholdTrackbar2.Value;
                threshold1Value.Text = Convert.ToString(thresholdTrackbar1.Value);
            }
            threshold2Value.Text = Convert.ToString(imgData.HistogramVerticalManualSegmentation(mode, thresholdTrackbar1.Value, thresholdTrackbar2.Value)[1]);
            pictureBox2.Image = imgData.DrawImage(mode, histogramNew);
        }

        private void greenTwoVerticalManualThresholds_Click(object sender, EventArgs e)
        {
            VerticalManualThresholdClick("GREEN", true);
        }

        private void blueTwoVerticalManualThresholds_Click(object sender, EventArgs e)
        {
            VerticalManualThresholdClick("BLUE", true);
        }

        private void intensityTwoVerticalManualThresholds_Click(object sender, EventArgs e)
        {
            VerticalManualThresholdClick("INTENSITY", true);
        }

        private void transitionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Transitions transitions = new Transitions();
            transitions.Show();
        }

        private void transformationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Transformations transformations = new Transformations();
            transformations.Show();

        }
    }
}
