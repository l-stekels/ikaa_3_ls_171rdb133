﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ikaa_8_ls_171rdb133
{
    public partial class Transformations : Form
    {
        private ImgData imgData = new ImgData();
        private ImgData imgData2 = new ImgData();
        public Transformations()
        {
            InitializeComponent();
        }

        public bool ImagesAreValid()
        {
            if (imgData.img == null)
            {
                MessageBox.Show("Image not loaded", "ERROR");
                return false;
            }

            return true;
        }

        private void Translate(int x, int y)
        {
            for (int a = 0; a < imgData.img.GetLength(0); a++)
            {
                for (int b = 0; b < imgData.img.GetLength(1); b++)
                {
                    imgData.img[a, b].X += x;
                    imgData.img[a, b].Y += y;
                }
            }
        }

        private void Rotate(int angle)
        {
            for (int x = 0; x < imgData.img.GetLength(0); x++)
            {
                for (int y = 0; y < imgData.img.GetLength(1); y++)
                {
                    int X = imgData.img[x, y].X;
                    int Y = imgData.img[x, y].Y;
                    imgData.img[x, y].X = (int)Math.Round(X * Math.Cos(Math.PI / 180 * angle) - Y * Math.Sin(Math.PI / 180 * angle));
                    imgData.img[x, y].Y = (int)Math.Round(X * Math.Sin(Math.PI / 180 * angle) + Y * Math.Cos(Math.PI / 180 * angle));
                }
            }
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
                Bitmap bmp = (Bitmap)pictureBox1.Image.Clone();
                imgData.ReadImage(bmp);
                imgData2.ReadImage(bmp);
                pictureBox1.Image = imgData.DrawImage();
                GC.Collect();
            }
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            if (imgData.img == null)
            {
                return;
            }
            for (int x = 0; x < imgData2.img.GetLength(0); x++)
            {
                for (int y = 0; y < imgData2.img.GetLength(1); y++)
                {
                    PixelClassRGB rgb = new PixelClassRGB(
                        imgData2.img[x, y].R,
                        imgData2.img[x, y].G,
                        imgData2.img[x, y].B
                    );
                    imgData.img[x, y] = rgb;
                    imgData.img[x, y].X = (imgData2.img[x, y].X);
                    imgData.img[x, y].Y = (imgData2.img[x, y].Y);
                }
            }
            imgData.x0 = imgData2.img.GetLength(0) / 2;
            imgData.y0 = imgData2.img.GetLength(1) / 2;
            pictureBox1.Image = imgData.DrawImage("RGB");
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (this.saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.pictureBox1.Image.Save(this.saveFileDialog1.FileName);
            }
        }

        private void translateButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            Translate(Convert.ToInt32(translateX.Text), Convert.ToInt32(translateY.Text));
            pictureBox1.Image = imgData.DrawTransformedImage();
        }

        private void rotateButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            Rotate(Convert.ToInt32(rotationAngle.Text));
            pictureBox1.Image = imgData.DrawTransformedImage();
        }

        private void skewButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            Skew(Convert.ToDouble(skewX.Text), Convert.ToDouble(skewY.Text));
            pictureBox1.Image = imgData.DrawTransformedImage();
        }

        private void Skew(double v1, double v2)
        {
            for (int x = 0; x < imgData.img.GetLength(0); x++)
            {
                for (int y = 0; y < imgData.img.GetLength(1); y++)
                {
                    int X = imgData.img[x, y].X;
                    int Y = imgData.img[x, y].Y;
                    imgData.img[x, y].X = (int)Math.Round(X + (v1 * Y));
                    imgData.img[x, y].Y = (int)Math.Round(Y + (v2 * X));
                }
            }
        }

        private void waveButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            Wave(Convert.ToInt32(waveX.Text), Convert.ToInt32(waveY.Text));
            pictureBox1.Image = imgData.DrawTransformedImage();
        }

        private void Wave(int v1, int v2)
        {
            for (int x = 0; x < imgData.img.GetLength(0); x++)
            {
                for (int y = 0; y < imgData.img.GetLength(1); y++)
                {
                    int X = imgData.img[x, y].X;
                    int Y = imgData.img[x, y].Y;
                    imgData.img[x, y].X = (int)Math.Round(X + (v1 * Math.Sin(2 * Math.PI * Y / 128.0)));
                    imgData.img[x, y].Y = (int)Math.Round(Y + (v2 * Math.Sin(2 * Math.PI * X / 128.0)));
                }
            }
        }

        private void warpButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            Warp();
            pictureBox1.Image = imgData.DrawTransformedImage();
        }

        private void Warp()
        {
            int xHalf = imgData.img.GetLength(0) / 2;

            for (int x = 0; x < imgData.img.GetLength(0); x++)
            {
                for (int y = 0; y < imgData.img.GetLength(1); y++)
                {
                    int Z = imgData.img[x, y].X + xHalf;
                    int Y = imgData.img[x, y].Y;
                    imgData.img[x, y].X = ((int)Math.Round((double)(((Math.Sign(Z - xHalf) * Math.Pow(Z - xHalf, 2.0)) / xHalf) + xHalf))) - xHalf;
                    imgData.img[x, y].Y = Y;
                }
            }
        }

        private void swirlButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            Swirl();
            pictureBox1.Image = imgData.DrawTransformedImage();
        }

        private void Swirl()
        {
            int xHalf = imgData.img.GetLength(0) / 2;
            int yHalf = imgData.img.GetLength(1) / 2;

            for (int x = 0; x < imgData.img.GetLength(0); x++)
            {
                for (int y = 0; y < imgData.img.GetLength(1); y++)
                {
                    int X = imgData.img[x, y].X + xHalf;
                    int Y = imgData.img[x, y].Y + yHalf;
                    double Z = Math.PI * ((float)Math.Sqrt(Math.Pow(X - xHalf, 2.0) + Math.Pow(Y - yHalf, 2.0))) / imgData.img.GetLength(0);
                    imgData.img[x, y].X = ((int)Math.Round((double)(((X - xHalf) * Math.Cos(Z)) + ((Y - yHalf) * Math.Sin(Z)) + xHalf))) - xHalf;
                    imgData.img[x, y].Y = ((int)Math.Round((double)((-(X - xHalf) * Math.Sin(Z)) + ((Y - yHalf) * Math.Cos(Z)) + yHalf))) - yHalf;
                }
            }
        }
    }
}
