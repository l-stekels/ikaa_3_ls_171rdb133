﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ikaa_8_ls_171rdb133
{
    public partial class Transitions : Form
    {
        public ImgData imgData1 = new ImgData();
        public ImgData imgData2 = new ImgData(); 
        public ImgData imgData3 = new ImgData();
        public Effects effects;
        public int transitionStep;
        public Transitions()
        {
            InitializeComponent();
            effects = new Effects(this);
        }

        public bool ImagesAreValid()
        {
            if (imgData1.img == null || imgData2.img == null)
            {
                MessageBox.Show("Both images need to be loaded", "ERROR");
                return false;
            }
            if (imgData1.img.GetLength(0) != imgData2.img.GetLength(0) || imgData1.img.GetLength(1) != imgData2.img.GetLength(1))
            {
                MessageBox.Show("Both images need to have the same size", "ERROR");
                return false;
            }
            return true;
        }

        public void Transition(int step)
        {
            Transition transition = new Transition(TransitionType(), imgData1.img, imgData2.img);
            for (int x = 0; x < imgData1.img.GetLength(0); x++)
            {
                for (int y = 0; y < imgData1.img.GetLength(1); y++)
                {
                    imgData3.imgNew[x, y] = transition.GetPixel(step, x, y);
                }
            }
            resultPicture.Image = imgData3.DrawImage();
        }

        private string TransitionType()
        {
            if (wipeRadio.Checked)
            {
                return "WIPE";
            }
            if (swipeRadio.Checked)
            {
                return "SWIPE";
            }
            if (circleRadio.Checked)
            {
                return "CIRCLE";
            }
            if (squareRadio.Checked)
            {
                return "SQUARE";
            }
            return "FADE";
        }

        private void picture1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                picture1.Image = Image.FromFile(openFileDialog1.FileName);
                Bitmap bmp = (Bitmap)picture1.Image.Clone();
                imgData1.ReadImage(bmp);
                picture1.Image = imgData1.DrawImage();
                GC.Collect();
            }
        }

        private void picture2_Click(object sender, EventArgs e)
        {
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                picture2.Image = Image.FromFile(openFileDialog2.FileName);
                Bitmap bmp = (Bitmap)picture2.Image.Clone();
                imgData2.ReadImage(bmp);
                picture2.Image = imgData2.DrawImage();
                imgData3.imgNew = new PixelClassRGB[imgData1.img.GetLength(0), imgData1.img.GetLength(1)];
                GC.Collect();
            }
        }

        private void opacityTrackBar_Scroll(object sender, EventArgs e)
        {
            opacityValue.Text = opacityTrackBar.Value.ToString() + " %";
            effects.Opacity(opacityTrackBar.Value);
            resultPicture.Image = imgData3.DrawImage();
        }

        private void opacityButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.Opacity(opacityTrackBar.Value);
            resultPicture.Image = imgData3.DrawImage();
        }

        private void screenButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.Screen();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void dodgeButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.Dodge();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void overlayButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.Overlay();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void burnButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.Burn();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void softLightButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.SoftLight();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void hardLightButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.HardLight();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void lightenButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.Lighten();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void darkenButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.Darken();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void additionButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.Addition();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void subtractionButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.Subtraction();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void multiplyButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.Multipy();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void differenceButton_Click(object sender, EventArgs e)
        {
            if (!ImagesAreValid())
            {
                return;
            }
            effects.Difference();
            resultPicture.Image = imgData3.DrawImage();
        }

        private void transitionButton_Click(object sender, EventArgs e)
        {
            if(!ImagesAreValid())
            {
                return;
            }
            transitionStep = 0;
            timer1.Enabled = true;
            groupBoxEffects.Enabled = false;
            groupBoxOpacity.Enabled = false;
            groupBoxTransitions.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            transitionStep++;
            if (transitionStep > 100)
            {
                timer1.Enabled = false;
                groupBoxOpacity.Enabled = true;
                groupBoxEffects.Enabled = true;
                groupBoxTransitions.Enabled = true;
                return;
            }
            progressBar.Value = transitionStep;
            Transition(transitionStep);
        }
    }
}
