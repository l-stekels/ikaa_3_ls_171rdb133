﻿namespace ikaa_8_ls_171rdb133
{
    partial class Transformations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.translateX = new System.Windows.Forms.TextBox();
            this.translateY = new System.Windows.Forms.TextBox();
            this.rotationAngle = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.skewY = new System.Windows.Forms.TextBox();
            this.skewX = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.waveY = new System.Windows.Forms.TextBox();
            this.waveX = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.translateButton = new System.Windows.Forms.Button();
            this.rotateButton = new System.Windows.Forms.Button();
            this.skewButton = new System.Windows.Forms.Button();
            this.waveButton = new System.Windows.Forms.Button();
            this.warpButton = new System.Windows.Forms.Button();
            this.swirlButton = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(430, 430);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(448, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(448, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Y";
            // 
            // translateX
            // 
            this.translateX.Location = new System.Drawing.Point(488, 13);
            this.translateX.Name = "translateX";
            this.translateX.Size = new System.Drawing.Size(25, 20);
            this.translateX.TabIndex = 3;
            this.translateX.Text = "0";
            // 
            // translateY
            // 
            this.translateY.Location = new System.Drawing.Point(488, 39);
            this.translateY.Name = "translateY";
            this.translateY.Size = new System.Drawing.Size(25, 20);
            this.translateY.TabIndex = 4;
            this.translateY.Text = "0";
            // 
            // rotationAngle
            // 
            this.rotationAngle.Location = new System.Drawing.Point(489, 79);
            this.rotationAngle.Name = "rotationAngle";
            this.rotationAngle.Size = new System.Drawing.Size(25, 20);
            this.rotationAngle.TabIndex = 7;
            this.rotationAngle.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(449, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Angle";
            // 
            // skewY
            // 
            this.skewY.Location = new System.Drawing.Point(488, 144);
            this.skewY.Name = "skewY";
            this.skewY.Size = new System.Drawing.Size(25, 20);
            this.skewY.TabIndex = 12;
            this.skewY.Text = "0";
            // 
            // skewX
            // 
            this.skewX.Location = new System.Drawing.Point(488, 118);
            this.skewX.Name = "skewX";
            this.skewX.Size = new System.Drawing.Size(25, 20);
            this.skewX.TabIndex = 11;
            this.skewX.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(448, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Y";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(448, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "X";
            // 
            // waveY
            // 
            this.waveY.Location = new System.Drawing.Point(488, 197);
            this.waveY.Name = "waveY";
            this.waveY.Size = new System.Drawing.Size(25, 20);
            this.waveY.TabIndex = 16;
            this.waveY.Text = "0";
            // 
            // waveX
            // 
            this.waveX.Location = new System.Drawing.Point(488, 171);
            this.waveX.Name = "waveX";
            this.waveX.Size = new System.Drawing.Size(25, 20);
            this.waveX.TabIndex = 15;
            this.waveX.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(448, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Y";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(448, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "X";
            // 
            // translateButton
            // 
            this.translateButton.Location = new System.Drawing.Point(520, 12);
            this.translateButton.Name = "translateButton";
            this.translateButton.Size = new System.Drawing.Size(158, 47);
            this.translateButton.TabIndex = 17;
            this.translateButton.Text = "Translate";
            this.translateButton.UseVisualStyleBackColor = true;
            this.translateButton.Click += new System.EventHandler(this.translateButton_Click);
            // 
            // rotateButton
            // 
            this.rotateButton.Location = new System.Drawing.Point(520, 65);
            this.rotateButton.Name = "rotateButton";
            this.rotateButton.Size = new System.Drawing.Size(158, 47);
            this.rotateButton.TabIndex = 18;
            this.rotateButton.Text = "Rotate";
            this.rotateButton.UseVisualStyleBackColor = true;
            this.rotateButton.Click += new System.EventHandler(this.rotateButton_Click);
            // 
            // skewButton
            // 
            this.skewButton.Location = new System.Drawing.Point(520, 118);
            this.skewButton.Name = "skewButton";
            this.skewButton.Size = new System.Drawing.Size(158, 47);
            this.skewButton.TabIndex = 19;
            this.skewButton.Text = "Skew";
            this.skewButton.UseVisualStyleBackColor = true;
            this.skewButton.Click += new System.EventHandler(this.skewButton_Click);
            // 
            // waveButton
            // 
            this.waveButton.Location = new System.Drawing.Point(520, 171);
            this.waveButton.Name = "waveButton";
            this.waveButton.Size = new System.Drawing.Size(158, 47);
            this.waveButton.TabIndex = 20;
            this.waveButton.Text = "Wave";
            this.waveButton.UseVisualStyleBackColor = true;
            this.waveButton.Click += new System.EventHandler(this.waveButton_Click);
            // 
            // warpButton
            // 
            this.warpButton.Location = new System.Drawing.Point(520, 224);
            this.warpButton.Name = "warpButton";
            this.warpButton.Size = new System.Drawing.Size(158, 47);
            this.warpButton.TabIndex = 21;
            this.warpButton.Text = "Warp";
            this.warpButton.UseVisualStyleBackColor = true;
            this.warpButton.Click += new System.EventHandler(this.warpButton_Click);
            // 
            // swirlButton
            // 
            this.swirlButton.Location = new System.Drawing.Point(520, 277);
            this.swirlButton.Name = "swirlButton";
            this.swirlButton.Size = new System.Drawing.Size(158, 47);
            this.swirlButton.TabIndex = 22;
            this.swirlButton.Text = "Swirl";
            this.swirlButton.UseVisualStyleBackColor = true;
            this.swirlButton.Click += new System.EventHandler(this.swirlButton_Click);
            // 
            // resetButton
            // 
            this.resetButton.BackColor = System.Drawing.Color.Tomato;
            this.resetButton.Location = new System.Drawing.Point(452, 330);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(217, 47);
            this.resetButton.TabIndex = 23;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = false;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.Lime;
            this.saveButton.Location = new System.Drawing.Point(452, 383);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(217, 47);
            this.saveButton.TabIndex = 24;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // Transformations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 455);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.swirlButton);
            this.Controls.Add(this.warpButton);
            this.Controls.Add(this.waveButton);
            this.Controls.Add(this.skewButton);
            this.Controls.Add(this.rotateButton);
            this.Controls.Add(this.translateButton);
            this.Controls.Add(this.waveY);
            this.Controls.Add(this.waveX);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.skewY);
            this.Controls.Add(this.skewX);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.rotationAngle);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.translateY);
            this.Controls.Add(this.translateX);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Transformations";
            this.Text = "Transformations";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox translateX;
        private System.Windows.Forms.TextBox translateY;
        private System.Windows.Forms.TextBox rotationAngle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox skewY;
        private System.Windows.Forms.TextBox skewX;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox waveY;
        private System.Windows.Forms.TextBox waveX;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button translateButton;
        private System.Windows.Forms.Button rotateButton;
        private System.Windows.Forms.Button skewButton;
        private System.Windows.Forms.Button waveButton;
        private System.Windows.Forms.Button warpButton;
        private System.Windows.Forms.Button swirlButton;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}