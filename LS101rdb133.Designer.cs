﻿namespace ikaa_8_ls_171rdb133
{
    partial class LS_171rdb133
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.colorSystems = new System.Windows.Forms.GroupBox();
            this.radioYUV = new System.Windows.Forms.RadioButton();
            this.radioCMYK = new System.Windows.Forms.RadioButton();
            this.radioHSV = new System.Windows.Forms.RadioButton();
            this.radioRGB = new System.Windows.Forms.RadioButton();
            this.channels = new System.Windows.Forms.GroupBox();
            this.colorChannel4 = new System.Windows.Forms.RadioButton();
            this.colorChannel3 = new System.Windows.Forms.RadioButton();
            this.colorChannel2 = new System.Windows.Forms.RadioButton();
            this.colorChannel1 = new System.Windows.Forms.RadioButton();
            this.compositeColors = new System.Windows.Forms.RadioButton();
            this.red = new System.Windows.Forms.Label();
            this.redValue = new System.Windows.Forms.Label();
            this.green = new System.Windows.Forms.Label();
            this.greenValue = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.blueValue = new System.Windows.Forms.Label();
            this.valueValue = new System.Windows.Forms.Label();
            this.value = new System.Windows.Forms.Label();
            this.saturationValue = new System.Windows.Forms.Label();
            this.saturation = new System.Windows.Forms.Label();
            this.hueValue = new System.Windows.Forms.Label();
            this.hue = new System.Windows.Forms.Label();
            this.yellowValue = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.magentaValue = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cyanValue = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.keyValue = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.vValue = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uValue = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.yValue = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.xPosition = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.yPosition = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.histogramOriginal = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.histogramNew = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drawingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filtersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blurlinearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blur19ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blur110ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blur116ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mediannonlinearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.median3x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.median5x5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.median7x7ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharpenlinearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharpen1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharpen2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharpen3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.segmentationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edgeDetectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.soboelOperator3x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rGBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intensityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prewittOperator3x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rGBToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.redToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.greenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.blueToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.intensityToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.robertsOperator2x2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rGBToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.redToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.greenToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.blueToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.intensityToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.verticalAdaptiveThresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redVerticalAdaptiveThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.greenVerticalAdaptiveThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.blueVerticalAdaptiveThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.intensityVerticalAdaptiveThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.verticalManualThresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redVerticalManualThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.greenVerticalManualThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.blueVerticalManualThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.intensityVerticalManualThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontalManualThresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redHorizontalManualThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.greenHorizontalManualThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.blueHorizontalManualThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.intensityHorizontalManualThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.twoVerticalManualThresholdsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redTwoVerticalManualThresholds = new System.Windows.Forms.ToolStripMenuItem();
            this.greenTwoVerticalManualThresholds = new System.Windows.Forms.ToolStripMenuItem();
            this.blueTwoVerticalManualThresholds = new System.Windows.Forms.ToolStripMenuItem();
            this.intensityTwoVerticalManualThresholds = new System.Windows.Forms.ToolStripMenuItem();
            this.transitionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.normalizeValueLabel = new System.Windows.Forms.Label();
            this.normalizeLabel = new System.Windows.Forms.Label();
            this.normalizeValue = new System.Windows.Forms.TrackBar();
            this.normalize = new System.Windows.Forms.RadioButton();
            this.stretch = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.thresholdTrackbar2 = new System.Windows.Forms.TrackBar();
            this.threshold2Value = new System.Windows.Forms.Label();
            this.threshold2Label = new System.Windows.Forms.Label();
            this.threshold1Value = new System.Windows.Forms.Label();
            this.threshold1Label = new System.Windows.Forms.Label();
            this.thresholdTrackbar1 = new System.Windows.Forms.TrackBar();
            this.transformationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.colorSystems.SuspendLayout();
            this.channels.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.histogramOriginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.histogramNew)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.normalizeValue)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdTrackbar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdTrackbar1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox1.Location = new System.Drawing.Point(4, 30);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(388, 404);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBox1_MouseMove);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pictureBox2.Location = new System.Drawing.Point(401, 30);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(388, 404);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PictureBox2_MouseClick);
            // 
            // colorSystems
            // 
            this.colorSystems.Controls.Add(this.radioYUV);
            this.colorSystems.Controls.Add(this.radioCMYK);
            this.colorSystems.Controls.Add(this.radioHSV);
            this.colorSystems.Controls.Add(this.radioRGB);
            this.colorSystems.Location = new System.Drawing.Point(806, 30);
            this.colorSystems.Margin = new System.Windows.Forms.Padding(2);
            this.colorSystems.Name = "colorSystems";
            this.colorSystems.Padding = new System.Windows.Forms.Padding(2);
            this.colorSystems.Size = new System.Drawing.Size(96, 107);
            this.colorSystems.TabIndex = 2;
            this.colorSystems.TabStop = false;
            this.colorSystems.Text = "Color system";
            // 
            // radioYUV
            // 
            this.radioYUV.AutoSize = true;
            this.radioYUV.Location = new System.Drawing.Point(8, 81);
            this.radioYUV.Margin = new System.Windows.Forms.Padding(2);
            this.radioYUV.Name = "radioYUV";
            this.radioYUV.Size = new System.Drawing.Size(47, 17);
            this.radioYUV.TabIndex = 3;
            this.radioYUV.TabStop = true;
            this.radioYUV.Text = "YUV";
            this.radioYUV.UseVisualStyleBackColor = true;
            this.radioYUV.CheckedChanged += new System.EventHandler(this.RadioYUV_CheckedChanged);
            // 
            // radioCMYK
            // 
            this.radioCMYK.AutoSize = true;
            this.radioCMYK.Location = new System.Drawing.Point(8, 62);
            this.radioCMYK.Margin = new System.Windows.Forms.Padding(2);
            this.radioCMYK.Name = "radioCMYK";
            this.radioCMYK.Size = new System.Drawing.Size(55, 17);
            this.radioCMYK.TabIndex = 2;
            this.radioCMYK.TabStop = true;
            this.radioCMYK.Text = "CMYK";
            this.radioCMYK.UseVisualStyleBackColor = true;
            this.radioCMYK.CheckedChanged += new System.EventHandler(this.RadioCMYK_CheckedChanged);
            // 
            // radioHSV
            // 
            this.radioHSV.AutoSize = true;
            this.radioHSV.Location = new System.Drawing.Point(8, 43);
            this.radioHSV.Margin = new System.Windows.Forms.Padding(2);
            this.radioHSV.Name = "radioHSV";
            this.radioHSV.Size = new System.Drawing.Size(47, 17);
            this.radioHSV.TabIndex = 1;
            this.radioHSV.Text = "HSV";
            this.radioHSV.UseVisualStyleBackColor = true;
            this.radioHSV.CheckedChanged += new System.EventHandler(this.RadioHSV_CheckedChanged);
            // 
            // radioRGB
            // 
            this.radioRGB.AutoSize = true;
            this.radioRGB.Checked = true;
            this.radioRGB.Location = new System.Drawing.Point(8, 24);
            this.radioRGB.Margin = new System.Windows.Forms.Padding(2);
            this.radioRGB.Name = "radioRGB";
            this.radioRGB.Size = new System.Drawing.Size(48, 17);
            this.radioRGB.TabIndex = 0;
            this.radioRGB.TabStop = true;
            this.radioRGB.Text = "RGB";
            this.radioRGB.UseVisualStyleBackColor = true;
            this.radioRGB.CheckedChanged += new System.EventHandler(this.RadioRGB_CheckedChanged);
            // 
            // channels
            // 
            this.channels.Controls.Add(this.colorChannel4);
            this.channels.Controls.Add(this.colorChannel3);
            this.channels.Controls.Add(this.colorChannel2);
            this.channels.Controls.Add(this.colorChannel1);
            this.channels.Controls.Add(this.compositeColors);
            this.channels.Location = new System.Drawing.Point(806, 141);
            this.channels.Margin = new System.Windows.Forms.Padding(2);
            this.channels.Name = "channels";
            this.channels.Padding = new System.Windows.Forms.Padding(2);
            this.channels.Size = new System.Drawing.Size(95, 118);
            this.channels.TabIndex = 3;
            this.channels.TabStop = false;
            this.channels.Text = "Channels";
            // 
            // colorChannel4
            // 
            this.colorChannel4.AutoSize = true;
            this.colorChannel4.Location = new System.Drawing.Point(4, 91);
            this.colorChannel4.Margin = new System.Windows.Forms.Padding(2);
            this.colorChannel4.Name = "colorChannel4";
            this.colorChannel4.Size = new System.Drawing.Size(64, 17);
            this.colorChannel4.TabIndex = 4;
            this.colorChannel4.TabStop = true;
            this.colorChannel4.Text = "Intensity";
            this.colorChannel4.UseVisualStyleBackColor = true;
            this.colorChannel4.CheckedChanged += new System.EventHandler(this.ColorChannel4_CheckedChanged);
            // 
            // colorChannel3
            // 
            this.colorChannel3.AutoSize = true;
            this.colorChannel3.Location = new System.Drawing.Point(4, 72);
            this.colorChannel3.Margin = new System.Windows.Forms.Padding(2);
            this.colorChannel3.Name = "colorChannel3";
            this.colorChannel3.Size = new System.Drawing.Size(46, 17);
            this.colorChannel3.TabIndex = 3;
            this.colorChannel3.Text = "Blue";
            this.colorChannel3.UseVisualStyleBackColor = true;
            this.colorChannel3.CheckedChanged += new System.EventHandler(this.ColorChannel3_CheckedChanged);
            // 
            // colorChannel2
            // 
            this.colorChannel2.AutoSize = true;
            this.colorChannel2.Location = new System.Drawing.Point(4, 54);
            this.colorChannel2.Margin = new System.Windows.Forms.Padding(2);
            this.colorChannel2.Name = "colorChannel2";
            this.colorChannel2.Size = new System.Drawing.Size(54, 17);
            this.colorChannel2.TabIndex = 2;
            this.colorChannel2.Text = "Green";
            this.colorChannel2.UseVisualStyleBackColor = true;
            this.colorChannel2.CheckedChanged += new System.EventHandler(this.ColorChannel2_CheckedChanged);
            // 
            // colorChannel1
            // 
            this.colorChannel1.AutoSize = true;
            this.colorChannel1.Location = new System.Drawing.Point(4, 35);
            this.colorChannel1.Margin = new System.Windows.Forms.Padding(2);
            this.colorChannel1.Name = "colorChannel1";
            this.colorChannel1.Size = new System.Drawing.Size(45, 17);
            this.colorChannel1.TabIndex = 1;
            this.colorChannel1.Text = "Red";
            this.colorChannel1.UseVisualStyleBackColor = true;
            this.colorChannel1.CheckedChanged += new System.EventHandler(this.ColorChannel1_CheckedChanged);
            // 
            // compositeColors
            // 
            this.compositeColors.AutoSize = true;
            this.compositeColors.Checked = true;
            this.compositeColors.Location = new System.Drawing.Point(4, 16);
            this.compositeColors.Margin = new System.Windows.Forms.Padding(2);
            this.compositeColors.Name = "compositeColors";
            this.compositeColors.Size = new System.Drawing.Size(74, 17);
            this.compositeColors.TabIndex = 0;
            this.compositeColors.TabStop = true;
            this.compositeColors.Text = "Composite";
            this.compositeColors.UseVisualStyleBackColor = true;
            this.compositeColors.CheckedChanged += new System.EventHandler(this.CompositeColors_CheckedChanged);
            // 
            // red
            // 
            this.red.AutoSize = true;
            this.red.Location = new System.Drawing.Point(5, 16);
            this.red.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.red.Name = "red";
            this.red.Size = new System.Drawing.Size(15, 13);
            this.red.TabIndex = 4;
            this.red.Text = "R";
            // 
            // redValue
            // 
            this.redValue.AutoSize = true;
            this.redValue.Location = new System.Drawing.Point(22, 16);
            this.redValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.redValue.Name = "redValue";
            this.redValue.Size = new System.Drawing.Size(25, 13);
            this.redValue.TabIndex = 5;
            this.redValue.Text = "255";
            // 
            // green
            // 
            this.green.AutoSize = true;
            this.green.Location = new System.Drawing.Point(49, 16);
            this.green.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.green.Name = "green";
            this.green.Size = new System.Drawing.Size(15, 13);
            this.green.TabIndex = 6;
            this.green.Text = "G";
            // 
            // greenValue
            // 
            this.greenValue.AutoSize = true;
            this.greenValue.Location = new System.Drawing.Point(66, 16);
            this.greenValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.greenValue.Name = "greenValue";
            this.greenValue.Size = new System.Drawing.Size(25, 13);
            this.greenValue.TabIndex = 7;
            this.greenValue.Text = "255";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "B";
            // 
            // blueValue
            // 
            this.blueValue.AutoSize = true;
            this.blueValue.Location = new System.Drawing.Point(109, 16);
            this.blueValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.blueValue.Name = "blueValue";
            this.blueValue.Size = new System.Drawing.Size(25, 13);
            this.blueValue.TabIndex = 9;
            this.blueValue.Text = "255";
            // 
            // valueValue
            // 
            this.valueValue.AutoSize = true;
            this.valueValue.Location = new System.Drawing.Point(109, 35);
            this.valueValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.valueValue.Name = "valueValue";
            this.valueValue.Size = new System.Drawing.Size(25, 13);
            this.valueValue.TabIndex = 15;
            this.valueValue.Text = "255";
            // 
            // value
            // 
            this.value.AutoSize = true;
            this.value.Location = new System.Drawing.Point(93, 35);
            this.value.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.value.Name = "value";
            this.value.Size = new System.Drawing.Size(14, 13);
            this.value.TabIndex = 14;
            this.value.Text = "V";
            // 
            // saturationValue
            // 
            this.saturationValue.AutoSize = true;
            this.saturationValue.Location = new System.Drawing.Point(66, 35);
            this.saturationValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.saturationValue.Name = "saturationValue";
            this.saturationValue.Size = new System.Drawing.Size(25, 13);
            this.saturationValue.TabIndex = 13;
            this.saturationValue.Text = "255";
            // 
            // saturation
            // 
            this.saturation.AutoSize = true;
            this.saturation.Location = new System.Drawing.Point(49, 35);
            this.saturation.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.saturation.Name = "saturation";
            this.saturation.Size = new System.Drawing.Size(14, 13);
            this.saturation.TabIndex = 12;
            this.saturation.Text = "S";
            // 
            // hueValue
            // 
            this.hueValue.AutoSize = true;
            this.hueValue.Location = new System.Drawing.Point(22, 35);
            this.hueValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.hueValue.Name = "hueValue";
            this.hueValue.Size = new System.Drawing.Size(25, 13);
            this.hueValue.TabIndex = 11;
            this.hueValue.Text = "255";
            // 
            // hue
            // 
            this.hue.AutoSize = true;
            this.hue.Location = new System.Drawing.Point(5, 35);
            this.hue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.hue.Name = "hue";
            this.hue.Size = new System.Drawing.Size(15, 13);
            this.hue.TabIndex = 10;
            this.hue.Text = "H";
            // 
            // yellowValue
            // 
            this.yellowValue.AutoSize = true;
            this.yellowValue.Location = new System.Drawing.Point(109, 55);
            this.yellowValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.yellowValue.Name = "yellowValue";
            this.yellowValue.Size = new System.Drawing.Size(25, 13);
            this.yellowValue.TabIndex = 21;
            this.yellowValue.Text = "255";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(93, 55);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Y";
            // 
            // magentaValue
            // 
            this.magentaValue.AutoSize = true;
            this.magentaValue.Location = new System.Drawing.Point(66, 55);
            this.magentaValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.magentaValue.Name = "magentaValue";
            this.magentaValue.Size = new System.Drawing.Size(25, 13);
            this.magentaValue.TabIndex = 19;
            this.magentaValue.Text = "255";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(49, 55);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "M";
            // 
            // cyanValue
            // 
            this.cyanValue.AutoSize = true;
            this.cyanValue.Location = new System.Drawing.Point(22, 55);
            this.cyanValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cyanValue.Name = "cyanValue";
            this.cyanValue.Size = new System.Drawing.Size(25, 13);
            this.cyanValue.TabIndex = 17;
            this.cyanValue.Text = "255";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 55);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "C";
            // 
            // keyValue
            // 
            this.keyValue.AutoSize = true;
            this.keyValue.Location = new System.Drawing.Point(1055, 76);
            this.keyValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.keyValue.Name = "keyValue";
            this.keyValue.Size = new System.Drawing.Size(25, 13);
            this.keyValue.TabIndex = 23;
            this.keyValue.Text = "255";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(133, 55);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "K";
            // 
            // vValue
            // 
            this.vValue.AutoSize = true;
            this.vValue.Location = new System.Drawing.Point(109, 75);
            this.vValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.vValue.Name = "vValue";
            this.vValue.Size = new System.Drawing.Size(25, 13);
            this.vValue.TabIndex = 29;
            this.vValue.Text = "255";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(93, 75);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "V";
            // 
            // uValue
            // 
            this.uValue.AutoSize = true;
            this.uValue.Location = new System.Drawing.Point(66, 75);
            this.uValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uValue.Name = "uValue";
            this.uValue.Size = new System.Drawing.Size(25, 13);
            this.uValue.TabIndex = 27;
            this.uValue.Text = "255";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(49, 75);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "U";
            // 
            // yValue
            // 
            this.yValue.AutoSize = true;
            this.yValue.Location = new System.Drawing.Point(22, 75);
            this.yValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.yValue.Name = "yValue";
            this.yValue.Size = new System.Drawing.Size(25, 13);
            this.yValue.TabIndex = 25;
            this.yValue.Text = "255";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 75);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "Y";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 91);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "X";
            // 
            // xPosition
            // 
            this.xPosition.AutoSize = true;
            this.xPosition.Location = new System.Drawing.Point(22, 91);
            this.xPosition.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.xPosition.Name = "xPosition";
            this.xPosition.Size = new System.Drawing.Size(13, 13);
            this.xPosition.TabIndex = 31;
            this.xPosition.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(50, 91);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(14, 13);
            this.label18.TabIndex = 32;
            this.label18.Text = "Y";
            // 
            // yPosition
            // 
            this.yPosition.AutoSize = true;
            this.yPosition.Location = new System.Drawing.Point(67, 91);
            this.yPosition.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.yPosition.Name = "yPosition";
            this.yPosition.Size = new System.Drawing.Size(13, 13);
            this.yPosition.TabIndex = 33;
            this.yPosition.Text = "0";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "jpg";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.SaveFileDialog1_FileOk);
            // 
            // histogramOriginal
            // 
            chartArea3.Name = "ChartArea1";
            this.histogramOriginal.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.histogramOriginal.Legends.Add(legend3);
            this.histogramOriginal.Location = new System.Drawing.Point(4, 439);
            this.histogramOriginal.Name = "histogramOriginal";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.histogramOriginal.Series.Add(series3);
            this.histogramOriginal.Size = new System.Drawing.Size(388, 148);
            this.histogramOriginal.TabIndex = 38;
            this.histogramOriginal.Text = "chart1";
            // 
            // histogramNew
            // 
            chartArea4.Name = "ChartArea1";
            this.histogramNew.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.histogramNew.Legends.Add(legend4);
            this.histogramNew.Location = new System.Drawing.Point(401, 439);
            this.histogramNew.Name = "histogramNew";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.histogramNew.Series.Add(series4);
            this.histogramNew.Size = new System.Drawing.Size(388, 148);
            this.histogramNew.TabIndex = 39;
            this.histogramNew.Text = "chart2";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.drawingToolStripMenuItem,
            this.filtersToolStripMenuItem,
            this.segmentationToolStripMenuItem,
            this.transitionsToolStripMenuItem,
            this.transformationsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1294, 24);
            this.menuStrip1.TabIndex = 40;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadImageToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.invertToolStripMenuItem,
            this.resetToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadImageToolStripMenuItem
            // 
            this.loadImageToolStripMenuItem.Name = "loadImageToolStripMenuItem";
            this.loadImageToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.loadImageToolStripMenuItem.Text = "Open";
            this.loadImageToolStripMenuItem.Click += new System.EventHandler(this.loadImageToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // invertToolStripMenuItem
            // 
            this.invertToolStripMenuItem.Name = "invertToolStripMenuItem";
            this.invertToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.invertToolStripMenuItem.Text = "Invert";
            this.invertToolStripMenuItem.Click += new System.EventHandler(this.invertToolStripMenuItem_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.resetToolStripMenuItem.Text = "Reset";
            // 
            // drawingToolStripMenuItem
            // 
            this.drawingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectColorToolStripMenuItem});
            this.drawingToolStripMenuItem.Name = "drawingToolStripMenuItem";
            this.drawingToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.drawingToolStripMenuItem.Text = "Drawing";
            // 
            // selectColorToolStripMenuItem
            // 
            this.selectColorToolStripMenuItem.Name = "selectColorToolStripMenuItem";
            this.selectColorToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.selectColorToolStripMenuItem.Text = "Select Color";
            this.selectColorToolStripMenuItem.Click += new System.EventHandler(this.selectColorToolStripMenuItem_Click);
            // 
            // filtersToolStripMenuItem
            // 
            this.filtersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blurlinearToolStripMenuItem,
            this.mediannonlinearToolStripMenuItem,
            this.sharpenlinearToolStripMenuItem});
            this.filtersToolStripMenuItem.Name = "filtersToolStripMenuItem";
            this.filtersToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.filtersToolStripMenuItem.Text = "Filters";
            // 
            // blurlinearToolStripMenuItem
            // 
            this.blurlinearToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blur19ToolStripMenuItem,
            this.blur110ToolStripMenuItem,
            this.blur116ToolStripMenuItem});
            this.blurlinearToolStripMenuItem.Name = "blurlinearToolStripMenuItem";
            this.blurlinearToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.blurlinearToolStripMenuItem.Text = "Blur (linear)";
            // 
            // blur19ToolStripMenuItem
            // 
            this.blur19ToolStripMenuItem.Name = "blur19ToolStripMenuItem";
            this.blur19ToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.blur19ToolStripMenuItem.Text = "Blur (1/9)";
            this.blur19ToolStripMenuItem.Click += new System.EventHandler(this.blur19ToolStripMenuItem_Click);
            // 
            // blur110ToolStripMenuItem
            // 
            this.blur110ToolStripMenuItem.Name = "blur110ToolStripMenuItem";
            this.blur110ToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.blur110ToolStripMenuItem.Text = "Blur (1/10)";
            this.blur110ToolStripMenuItem.Click += new System.EventHandler(this.blur110ToolStripMenuItem_Click);
            // 
            // blur116ToolStripMenuItem
            // 
            this.blur116ToolStripMenuItem.Name = "blur116ToolStripMenuItem";
            this.blur116ToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.blur116ToolStripMenuItem.Text = "Blur (1/16)";
            this.blur116ToolStripMenuItem.Click += new System.EventHandler(this.blur116ToolStripMenuItem_Click);
            // 
            // mediannonlinearToolStripMenuItem
            // 
            this.mediannonlinearToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.median3x3ToolStripMenuItem,
            this.median5x5ToolStripMenuItem,
            this.median7x7ToolStripMenuItem});
            this.mediannonlinearToolStripMenuItem.Name = "mediannonlinearToolStripMenuItem";
            this.mediannonlinearToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.mediannonlinearToolStripMenuItem.Text = "Median (non-linear)";
            // 
            // median3x3ToolStripMenuItem
            // 
            this.median3x3ToolStripMenuItem.Name = "median3x3ToolStripMenuItem";
            this.median3x3ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.median3x3ToolStripMenuItem.Text = "Median (3x3)";
            this.median3x3ToolStripMenuItem.Click += new System.EventHandler(this.median3x3ToolStripMenuItem_Click);
            // 
            // median5x5ToolStripMenuItem
            // 
            this.median5x5ToolStripMenuItem.Name = "median5x5ToolStripMenuItem";
            this.median5x5ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.median5x5ToolStripMenuItem.Text = "Median (5x5)";
            this.median5x5ToolStripMenuItem.Click += new System.EventHandler(this.median5x5ToolStripMenuItem_Click);
            // 
            // median7x7ToolStripMenuItem
            // 
            this.median7x7ToolStripMenuItem.Name = "median7x7ToolStripMenuItem";
            this.median7x7ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.median7x7ToolStripMenuItem.Text = "Median (7x7)";
            this.median7x7ToolStripMenuItem.Click += new System.EventHandler(this.median7x7ToolStripMenuItem_Click);
            // 
            // sharpenlinearToolStripMenuItem
            // 
            this.sharpenlinearToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sharpen1ToolStripMenuItem,
            this.sharpen2ToolStripMenuItem,
            this.sharpen3ToolStripMenuItem});
            this.sharpenlinearToolStripMenuItem.Name = "sharpenlinearToolStripMenuItem";
            this.sharpenlinearToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sharpenlinearToolStripMenuItem.Text = "Sharpen (linear)";
            // 
            // sharpen1ToolStripMenuItem
            // 
            this.sharpen1ToolStripMenuItem.Name = "sharpen1ToolStripMenuItem";
            this.sharpen1ToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.sharpen1ToolStripMenuItem.Text = "Sharpen 1 (-1 5 -1)";
            this.sharpen1ToolStripMenuItem.Click += new System.EventHandler(this.sharpen1ToolStripMenuItem_Click);
            // 
            // sharpen2ToolStripMenuItem
            // 
            this.sharpen2ToolStripMenuItem.Name = "sharpen2ToolStripMenuItem";
            this.sharpen2ToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.sharpen2ToolStripMenuItem.Text = "Sharpen 2 (-1 9 -1)";
            this.sharpen2ToolStripMenuItem.Click += new System.EventHandler(this.sharpen2ToolStripMenuItem_Click);
            // 
            // sharpen3ToolStripMenuItem
            // 
            this.sharpen3ToolStripMenuItem.Name = "sharpen3ToolStripMenuItem";
            this.sharpen3ToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.sharpen3ToolStripMenuItem.Text = "Sharpen 3 (-2 9 -2)";
            this.sharpen3ToolStripMenuItem.Click += new System.EventHandler(this.sharpen3ToolStripMenuItem_Click);
            // 
            // segmentationToolStripMenuItem
            // 
            this.segmentationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.edgeDetectionToolStripMenuItem,
            this.verticalAdaptiveThresholdToolStripMenuItem,
            this.verticalManualThresholdToolStripMenuItem,
            this.horizontalManualThresholdToolStripMenuItem,
            this.twoVerticalManualThresholdsToolStripMenuItem});
            this.segmentationToolStripMenuItem.Name = "segmentationToolStripMenuItem";
            this.segmentationToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.segmentationToolStripMenuItem.Text = "Segmentation";
            // 
            // edgeDetectionToolStripMenuItem
            // 
            this.edgeDetectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.soboelOperator3x3ToolStripMenuItem,
            this.prewittOperator3x3ToolStripMenuItem,
            this.robertsOperator2x2ToolStripMenuItem});
            this.edgeDetectionToolStripMenuItem.Name = "edgeDetectionToolStripMenuItem";
            this.edgeDetectionToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.edgeDetectionToolStripMenuItem.Text = "Edge Detection";
            // 
            // soboelOperator3x3ToolStripMenuItem
            // 
            this.soboelOperator3x3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rGBToolStripMenuItem,
            this.redToolStripMenuItem,
            this.greenToolStripMenuItem,
            this.blueToolStripMenuItem,
            this.intensityToolStripMenuItem});
            this.soboelOperator3x3ToolStripMenuItem.Name = "soboelOperator3x3ToolStripMenuItem";
            this.soboelOperator3x3ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.soboelOperator3x3ToolStripMenuItem.Text = "Soboel Operator (3x3)";
            // 
            // rGBToolStripMenuItem
            // 
            this.rGBToolStripMenuItem.Name = "rGBToolStripMenuItem";
            this.rGBToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.rGBToolStripMenuItem.Text = "RGB";
            this.rGBToolStripMenuItem.Click += new System.EventHandler(this.rGBToolStripMenuItem_Click);
            // 
            // redToolStripMenuItem
            // 
            this.redToolStripMenuItem.Name = "redToolStripMenuItem";
            this.redToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.redToolStripMenuItem.Text = "Red";
            this.redToolStripMenuItem.Click += new System.EventHandler(this.redToolStripMenuItem_Click);
            // 
            // greenToolStripMenuItem
            // 
            this.greenToolStripMenuItem.Name = "greenToolStripMenuItem";
            this.greenToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.greenToolStripMenuItem.Text = "Green";
            this.greenToolStripMenuItem.Click += new System.EventHandler(this.greenToolStripMenuItem_Click);
            // 
            // blueToolStripMenuItem
            // 
            this.blueToolStripMenuItem.Name = "blueToolStripMenuItem";
            this.blueToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.blueToolStripMenuItem.Text = "Blue";
            this.blueToolStripMenuItem.Click += new System.EventHandler(this.blueToolStripMenuItem_Click);
            // 
            // intensityToolStripMenuItem
            // 
            this.intensityToolStripMenuItem.Name = "intensityToolStripMenuItem";
            this.intensityToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.intensityToolStripMenuItem.Text = "Intensity";
            this.intensityToolStripMenuItem.Click += new System.EventHandler(this.intensityToolStripMenuItem_Click);
            // 
            // prewittOperator3x3ToolStripMenuItem
            // 
            this.prewittOperator3x3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rGBToolStripMenuItem1,
            this.redToolStripMenuItem1,
            this.greenToolStripMenuItem1,
            this.blueToolStripMenuItem1,
            this.intensityToolStripMenuItem1});
            this.prewittOperator3x3ToolStripMenuItem.Name = "prewittOperator3x3ToolStripMenuItem";
            this.prewittOperator3x3ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.prewittOperator3x3ToolStripMenuItem.Text = "Prewitt Operator (3x3)";
            // 
            // rGBToolStripMenuItem1
            // 
            this.rGBToolStripMenuItem1.Name = "rGBToolStripMenuItem1";
            this.rGBToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.rGBToolStripMenuItem1.Text = "RGB";
            this.rGBToolStripMenuItem1.Click += new System.EventHandler(this.rGBToolStripMenuItem1_Click);
            // 
            // redToolStripMenuItem1
            // 
            this.redToolStripMenuItem1.Name = "redToolStripMenuItem1";
            this.redToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.redToolStripMenuItem1.Text = "Red";
            this.redToolStripMenuItem1.Click += new System.EventHandler(this.redToolStripMenuItem1_Click);
            // 
            // greenToolStripMenuItem1
            // 
            this.greenToolStripMenuItem1.Name = "greenToolStripMenuItem1";
            this.greenToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.greenToolStripMenuItem1.Text = "Green";
            this.greenToolStripMenuItem1.Click += new System.EventHandler(this.greenToolStripMenuItem1_Click);
            // 
            // blueToolStripMenuItem1
            // 
            this.blueToolStripMenuItem1.Name = "blueToolStripMenuItem1";
            this.blueToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.blueToolStripMenuItem1.Text = "Blue";
            this.blueToolStripMenuItem1.Click += new System.EventHandler(this.blueToolStripMenuItem1_Click);
            // 
            // intensityToolStripMenuItem1
            // 
            this.intensityToolStripMenuItem1.Name = "intensityToolStripMenuItem1";
            this.intensityToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.intensityToolStripMenuItem1.Text = "Intensity";
            this.intensityToolStripMenuItem1.Click += new System.EventHandler(this.intensityToolStripMenuItem1_Click);
            // 
            // robertsOperator2x2ToolStripMenuItem
            // 
            this.robertsOperator2x2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rGBToolStripMenuItem2,
            this.redToolStripMenuItem2,
            this.greenToolStripMenuItem2,
            this.blueToolStripMenuItem2,
            this.intensityToolStripMenuItem2});
            this.robertsOperator2x2ToolStripMenuItem.Name = "robertsOperator2x2ToolStripMenuItem";
            this.robertsOperator2x2ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.robertsOperator2x2ToolStripMenuItem.Text = "Roberts Operator (2x2)";
            // 
            // rGBToolStripMenuItem2
            // 
            this.rGBToolStripMenuItem2.Name = "rGBToolStripMenuItem2";
            this.rGBToolStripMenuItem2.Size = new System.Drawing.Size(119, 22);
            this.rGBToolStripMenuItem2.Text = "RGB";
            this.rGBToolStripMenuItem2.Click += new System.EventHandler(this.rGBToolStripMenuItem2_Click);
            // 
            // redToolStripMenuItem2
            // 
            this.redToolStripMenuItem2.Name = "redToolStripMenuItem2";
            this.redToolStripMenuItem2.Size = new System.Drawing.Size(119, 22);
            this.redToolStripMenuItem2.Text = "Red";
            this.redToolStripMenuItem2.Click += new System.EventHandler(this.redToolStripMenuItem2_Click);
            // 
            // greenToolStripMenuItem2
            // 
            this.greenToolStripMenuItem2.Name = "greenToolStripMenuItem2";
            this.greenToolStripMenuItem2.Size = new System.Drawing.Size(119, 22);
            this.greenToolStripMenuItem2.Text = "Green";
            this.greenToolStripMenuItem2.Click += new System.EventHandler(this.greenToolStripMenuItem2_Click);
            // 
            // blueToolStripMenuItem2
            // 
            this.blueToolStripMenuItem2.Name = "blueToolStripMenuItem2";
            this.blueToolStripMenuItem2.Size = new System.Drawing.Size(119, 22);
            this.blueToolStripMenuItem2.Text = "Blue";
            this.blueToolStripMenuItem2.Click += new System.EventHandler(this.blueToolStripMenuItem2_Click);
            // 
            // intensityToolStripMenuItem2
            // 
            this.intensityToolStripMenuItem2.Name = "intensityToolStripMenuItem2";
            this.intensityToolStripMenuItem2.Size = new System.Drawing.Size(119, 22);
            this.intensityToolStripMenuItem2.Text = "Intensity";
            this.intensityToolStripMenuItem2.Click += new System.EventHandler(this.intensityToolStripMenuItem2_Click);
            // 
            // verticalAdaptiveThresholdToolStripMenuItem
            // 
            this.verticalAdaptiveThresholdToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redVerticalAdaptiveThreshold,
            this.greenVerticalAdaptiveThreshold,
            this.blueVerticalAdaptiveThreshold,
            this.intensityVerticalAdaptiveThreshold});
            this.verticalAdaptiveThresholdToolStripMenuItem.Name = "verticalAdaptiveThresholdToolStripMenuItem";
            this.verticalAdaptiveThresholdToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.verticalAdaptiveThresholdToolStripMenuItem.Text = "Vertical Adaptive Threshold";
            // 
            // redVerticalAdaptiveThreshold
            // 
            this.redVerticalAdaptiveThreshold.Name = "redVerticalAdaptiveThreshold";
            this.redVerticalAdaptiveThreshold.Size = new System.Drawing.Size(119, 22);
            this.redVerticalAdaptiveThreshold.Text = "Red";
            this.redVerticalAdaptiveThreshold.Click += new System.EventHandler(this.redVerticalAdaptiveThreshold_Click);
            // 
            // greenVerticalAdaptiveThreshold
            // 
            this.greenVerticalAdaptiveThreshold.Name = "greenVerticalAdaptiveThreshold";
            this.greenVerticalAdaptiveThreshold.Size = new System.Drawing.Size(119, 22);
            this.greenVerticalAdaptiveThreshold.Text = "Green";
            this.greenVerticalAdaptiveThreshold.Click += new System.EventHandler(this.greenVerticalAdaptiveThreshold_Click);
            // 
            // blueVerticalAdaptiveThreshold
            // 
            this.blueVerticalAdaptiveThreshold.Name = "blueVerticalAdaptiveThreshold";
            this.blueVerticalAdaptiveThreshold.Size = new System.Drawing.Size(119, 22);
            this.blueVerticalAdaptiveThreshold.Text = "Blue";
            this.blueVerticalAdaptiveThreshold.Click += new System.EventHandler(this.blueVerticalAdaptiveThreshold_Click);
            // 
            // intensityVerticalAdaptiveThreshold
            // 
            this.intensityVerticalAdaptiveThreshold.Name = "intensityVerticalAdaptiveThreshold";
            this.intensityVerticalAdaptiveThreshold.Size = new System.Drawing.Size(119, 22);
            this.intensityVerticalAdaptiveThreshold.Text = "Intensity";
            this.intensityVerticalAdaptiveThreshold.Click += new System.EventHandler(this.intensityVerticalAdaptiveThreshold_Click);
            // 
            // verticalManualThresholdToolStripMenuItem
            // 
            this.verticalManualThresholdToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redVerticalManualThreshold,
            this.greenVerticalManualThreshold,
            this.blueVerticalManualThreshold,
            this.intensityVerticalManualThreshold});
            this.verticalManualThresholdToolStripMenuItem.Name = "verticalManualThresholdToolStripMenuItem";
            this.verticalManualThresholdToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.verticalManualThresholdToolStripMenuItem.Text = "Vertical Manual Threshold";
            // 
            // redVerticalManualThreshold
            // 
            this.redVerticalManualThreshold.Name = "redVerticalManualThreshold";
            this.redVerticalManualThreshold.Size = new System.Drawing.Size(119, 22);
            this.redVerticalManualThreshold.Text = "Red";
            this.redVerticalManualThreshold.Click += new System.EventHandler(this.redVerticalManualThreshold_Click);
            // 
            // greenVerticalManualThreshold
            // 
            this.greenVerticalManualThreshold.Name = "greenVerticalManualThreshold";
            this.greenVerticalManualThreshold.Size = new System.Drawing.Size(119, 22);
            this.greenVerticalManualThreshold.Text = "Green";
            this.greenVerticalManualThreshold.Click += new System.EventHandler(this.greenVerticalManualThreshold_Click);
            // 
            // blueVerticalManualThreshold
            // 
            this.blueVerticalManualThreshold.Name = "blueVerticalManualThreshold";
            this.blueVerticalManualThreshold.Size = new System.Drawing.Size(119, 22);
            this.blueVerticalManualThreshold.Text = "Blue";
            this.blueVerticalManualThreshold.Click += new System.EventHandler(this.blueVerticalManualThreshold_Click);
            // 
            // intensityVerticalManualThreshold
            // 
            this.intensityVerticalManualThreshold.Name = "intensityVerticalManualThreshold";
            this.intensityVerticalManualThreshold.Size = new System.Drawing.Size(119, 22);
            this.intensityVerticalManualThreshold.Text = "Intensity";
            this.intensityVerticalManualThreshold.Click += new System.EventHandler(this.intensityVerticalManualThreshold_Click);
            // 
            // horizontalManualThresholdToolStripMenuItem
            // 
            this.horizontalManualThresholdToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redHorizontalManualThreshold,
            this.greenHorizontalManualThreshold,
            this.blueHorizontalManualThreshold,
            this.intensityHorizontalManualThreshold});
            this.horizontalManualThresholdToolStripMenuItem.Name = "horizontalManualThresholdToolStripMenuItem";
            this.horizontalManualThresholdToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.horizontalManualThresholdToolStripMenuItem.Text = "Horizontal Manual Threshold";
            // 
            // redHorizontalManualThreshold
            // 
            this.redHorizontalManualThreshold.Name = "redHorizontalManualThreshold";
            this.redHorizontalManualThreshold.Size = new System.Drawing.Size(119, 22);
            this.redHorizontalManualThreshold.Text = "Red";
            this.redHorizontalManualThreshold.Click += new System.EventHandler(this.redHorizontalManualThreshold_Click);
            // 
            // greenHorizontalManualThreshold
            // 
            this.greenHorizontalManualThreshold.Name = "greenHorizontalManualThreshold";
            this.greenHorizontalManualThreshold.Size = new System.Drawing.Size(119, 22);
            this.greenHorizontalManualThreshold.Text = "Green";
            this.greenHorizontalManualThreshold.Click += new System.EventHandler(this.greenHorizontalManualThreshold_Click);
            // 
            // blueHorizontalManualThreshold
            // 
            this.blueHorizontalManualThreshold.Name = "blueHorizontalManualThreshold";
            this.blueHorizontalManualThreshold.Size = new System.Drawing.Size(119, 22);
            this.blueHorizontalManualThreshold.Text = "Blue";
            this.blueHorizontalManualThreshold.Click += new System.EventHandler(this.blueHorizontalManualThreshold_Click);
            // 
            // intensityHorizontalManualThreshold
            // 
            this.intensityHorizontalManualThreshold.Name = "intensityHorizontalManualThreshold";
            this.intensityHorizontalManualThreshold.Size = new System.Drawing.Size(119, 22);
            this.intensityHorizontalManualThreshold.Text = "Intensity";
            this.intensityHorizontalManualThreshold.Click += new System.EventHandler(this.intensityHorizontalManualThreshold_Click);
            // 
            // twoVerticalManualThresholdsToolStripMenuItem
            // 
            this.twoVerticalManualThresholdsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redTwoVerticalManualThresholds,
            this.greenTwoVerticalManualThresholds,
            this.blueTwoVerticalManualThresholds,
            this.intensityTwoVerticalManualThresholds});
            this.twoVerticalManualThresholdsToolStripMenuItem.Name = "twoVerticalManualThresholdsToolStripMenuItem";
            this.twoVerticalManualThresholdsToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.twoVerticalManualThresholdsToolStripMenuItem.Text = "Two Vertical Manual Thresholds";
            // 
            // redTwoVerticalManualThresholds
            // 
            this.redTwoVerticalManualThresholds.Name = "redTwoVerticalManualThresholds";
            this.redTwoVerticalManualThresholds.Size = new System.Drawing.Size(119, 22);
            this.redTwoVerticalManualThresholds.Text = "Red";
            this.redTwoVerticalManualThresholds.Click += new System.EventHandler(this.redTwoVerticalManualThresholds_Click);
            // 
            // greenTwoVerticalManualThresholds
            // 
            this.greenTwoVerticalManualThresholds.Name = "greenTwoVerticalManualThresholds";
            this.greenTwoVerticalManualThresholds.Size = new System.Drawing.Size(119, 22);
            this.greenTwoVerticalManualThresholds.Text = "Green";
            this.greenTwoVerticalManualThresholds.Click += new System.EventHandler(this.greenTwoVerticalManualThresholds_Click);
            // 
            // blueTwoVerticalManualThresholds
            // 
            this.blueTwoVerticalManualThresholds.Name = "blueTwoVerticalManualThresholds";
            this.blueTwoVerticalManualThresholds.Size = new System.Drawing.Size(119, 22);
            this.blueTwoVerticalManualThresholds.Text = "Blue";
            this.blueTwoVerticalManualThresholds.Click += new System.EventHandler(this.blueTwoVerticalManualThresholds_Click);
            // 
            // intensityTwoVerticalManualThresholds
            // 
            this.intensityTwoVerticalManualThresholds.Name = "intensityTwoVerticalManualThresholds";
            this.intensityTwoVerticalManualThresholds.Size = new System.Drawing.Size(119, 22);
            this.intensityTwoVerticalManualThresholds.Text = "Intensity";
            this.intensityTwoVerticalManualThresholds.Click += new System.EventHandler(this.intensityTwoVerticalManualThresholds_Click);
            // 
            // transitionsToolStripMenuItem
            // 
            this.transitionsToolStripMenuItem.Name = "transitionsToolStripMenuItem";
            this.transitionsToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.transitionsToolStripMenuItem.Text = "Transitions";
            this.transitionsToolStripMenuItem.Click += new System.EventHandler(this.transitionsToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(906, 143);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 21);
            this.button1.TabIndex = 41;
            this.button1.Text = "Selected Color";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.normalizeValueLabel);
            this.groupBox1.Controls.Add(this.normalizeLabel);
            this.groupBox1.Controls.Add(this.normalizeValue);
            this.groupBox1.Controls.Add(this.normalize);
            this.groupBox1.Controls.Add(this.stretch);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Location = new System.Drawing.Point(806, 272);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(399, 119);
            this.groupBox1.TabIndex = 42;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Histogram";
            // 
            // normalizeValueLabel
            // 
            this.normalizeValueLabel.AutoSize = true;
            this.normalizeValueLabel.Location = new System.Drawing.Point(67, 79);
            this.normalizeValueLabel.Name = "normalizeValueLabel";
            this.normalizeValueLabel.Size = new System.Drawing.Size(21, 13);
            this.normalizeValueLabel.TabIndex = 5;
            this.normalizeValueLabel.Text = "0%";
            this.normalizeValueLabel.Visible = false;
            // 
            // normalizeLabel
            // 
            this.normalizeLabel.AutoSize = true;
            this.normalizeLabel.Location = new System.Drawing.Point(10, 79);
            this.normalizeLabel.Name = "normalizeLabel";
            this.normalizeLabel.Size = new System.Drawing.Size(46, 13);
            this.normalizeLabel.TabIndex = 4;
            this.normalizeLabel.Text = "Value = ";
            this.normalizeLabel.Visible = false;
            // 
            // normalizeValue
            // 
            this.normalizeValue.Location = new System.Drawing.Point(6, 42);
            this.normalizeValue.Maximum = 100;
            this.normalizeValue.Name = "normalizeValue";
            this.normalizeValue.Size = new System.Drawing.Size(377, 45);
            this.normalizeValue.TabIndex = 3;
            this.normalizeValue.Visible = false;
            this.normalizeValue.Scroll += new System.EventHandler(this.normalizeValue_Scroll);
            // 
            // normalize
            // 
            this.normalize.AutoSize = true;
            this.normalize.Location = new System.Drawing.Point(70, 19);
            this.normalize.Name = "normalize";
            this.normalize.Size = new System.Drawing.Size(71, 17);
            this.normalize.TabIndex = 2;
            this.normalize.Text = "Normalize";
            this.normalize.UseVisualStyleBackColor = true;
            this.normalize.CheckedChanged += new System.EventHandler(this.normalize_CheckedChanged);
            // 
            // stretch
            // 
            this.stretch.AutoSize = true;
            this.stretch.Checked = true;
            this.stretch.Location = new System.Drawing.Point(9, 19);
            this.stretch.Name = "stretch";
            this.stretch.Size = new System.Drawing.Size(59, 17);
            this.stretch.TabIndex = 1;
            this.stretch.TabStop = true;
            this.stretch.Text = "Stretch";
            this.stretch.UseVisualStyleBackColor = true;
            this.stretch.CheckedChanged += new System.EventHandler(this.stretch_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(227, 89);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(151, 26);
            this.button2.TabIndex = 0;
            this.button2.Text = "Contrast Enhancement";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.yPosition);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.xPosition);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.vValue);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.uValue);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.yValue);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.yellowValue);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.magentaValue);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cyanValue);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.valueValue);
            this.groupBox2.Controls.Add(this.value);
            this.groupBox2.Controls.Add(this.saturationValue);
            this.groupBox2.Controls.Add(this.saturation);
            this.groupBox2.Controls.Add(this.hueValue);
            this.groupBox2.Controls.Add(this.hue);
            this.groupBox2.Controls.Add(this.blueValue);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.greenValue);
            this.groupBox2.Controls.Add(this.green);
            this.groupBox2.Controls.Add(this.redValue);
            this.groupBox2.Controls.Add(this.red);
            this.groupBox2.Location = new System.Drawing.Point(907, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(172, 107);
            this.groupBox2.TabIndex = 43;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Image Data";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.thresholdTrackbar2);
            this.groupBox3.Controls.Add(this.threshold2Value);
            this.groupBox3.Controls.Add(this.threshold2Label);
            this.groupBox3.Controls.Add(this.threshold1Value);
            this.groupBox3.Controls.Add(this.threshold1Label);
            this.groupBox3.Controls.Add(this.thresholdTrackbar1);
            this.groupBox3.Location = new System.Drawing.Point(805, 397);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(400, 190);
            this.groupBox3.TabIndex = 43;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Segmentation";
            // 
            // thresholdTrackbar2
            // 
            this.thresholdTrackbar2.Location = new System.Drawing.Point(8, 107);
            this.thresholdTrackbar2.Maximum = 255;
            this.thresholdTrackbar2.Name = "thresholdTrackbar2";
            this.thresholdTrackbar2.Size = new System.Drawing.Size(377, 45);
            this.thresholdTrackbar2.TabIndex = 8;
            this.thresholdTrackbar2.Visible = false;
            this.thresholdTrackbar2.Scroll += new System.EventHandler(this.thresholdTrackbar2_Scroll);
            // 
            // threshold2Value
            // 
            this.threshold2Value.AutoSize = true;
            this.threshold2Value.Location = new System.Drawing.Point(154, 80);
            this.threshold2Value.Name = "threshold2Value";
            this.threshold2Value.Size = new System.Drawing.Size(13, 13);
            this.threshold2Value.TabIndex = 7;
            this.threshold2Value.Text = "0";
            this.threshold2Value.Visible = false;
            // 
            // threshold2Label
            // 
            this.threshold2Label.AutoSize = true;
            this.threshold2Label.Location = new System.Drawing.Point(7, 80);
            this.threshold2Label.Name = "threshold2Label";
            this.threshold2Label.Size = new System.Drawing.Size(142, 13);
            this.threshold2Label.TabIndex = 6;
            this.threshold2Label.Text = "Vertical Manual Threshold 2:";
            this.threshold2Label.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.threshold2Label.Visible = false;
            // 
            // threshold1Value
            // 
            this.threshold1Value.AutoSize = true;
            this.threshold1Value.Location = new System.Drawing.Point(152, 24);
            this.threshold1Value.Name = "threshold1Value";
            this.threshold1Value.Size = new System.Drawing.Size(13, 13);
            this.threshold1Value.TabIndex = 5;
            this.threshold1Value.Text = "0";
            this.threshold1Value.Visible = false;
            // 
            // threshold1Label
            // 
            this.threshold1Label.AutoSize = true;
            this.threshold1Label.Location = new System.Drawing.Point(7, 24);
            this.threshold1Label.Name = "threshold1Label";
            this.threshold1Label.Size = new System.Drawing.Size(142, 13);
            this.threshold1Label.TabIndex = 4;
            this.threshold1Label.Text = "Vertical Manual Threshold 1:";
            this.threshold1Label.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.threshold1Label.Visible = false;
            // 
            // thresholdTrackbar1
            // 
            this.thresholdTrackbar1.Location = new System.Drawing.Point(9, 42);
            this.thresholdTrackbar1.Maximum = 255;
            this.thresholdTrackbar1.Name = "thresholdTrackbar1";
            this.thresholdTrackbar1.Size = new System.Drawing.Size(377, 45);
            this.thresholdTrackbar1.TabIndex = 3;
            this.thresholdTrackbar1.Visible = false;
            this.thresholdTrackbar1.Scroll += new System.EventHandler(this.thresholdTrackbar1_Scroll);
            // 
            // transformationsToolStripMenuItem
            // 
            this.transformationsToolStripMenuItem.Name = "transformationsToolStripMenuItem";
            this.transformationsToolStripMenuItem.Size = new System.Drawing.Size(105, 20);
            this.transformationsToolStripMenuItem.Text = "Transformations";
            this.transformationsToolStripMenuItem.Click += new System.EventHandler(this.transformationsToolStripMenuItem_Click);
            // 
            // LS_171rdb133
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1294, 599);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.histogramNew);
            this.Controls.Add(this.histogramOriginal);
            this.Controls.Add(this.keyValue);
            this.Controls.Add(this.channels);
            this.Controls.Add(this.colorSystems);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "LS_171rdb133";
            this.Text = "LS_171rdb133";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.colorSystems.ResumeLayout(false);
            this.colorSystems.PerformLayout();
            this.channels.ResumeLayout(false);
            this.channels.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.histogramOriginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.histogramNew)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.normalizeValue)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdTrackbar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdTrackbar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox colorSystems;
        private System.Windows.Forms.RadioButton radioYUV;
        private System.Windows.Forms.RadioButton radioCMYK;
        private System.Windows.Forms.RadioButton radioHSV;
        private System.Windows.Forms.RadioButton radioRGB;
        private System.Windows.Forms.GroupBox channels;
        private System.Windows.Forms.Label red;
        private System.Windows.Forms.Label redValue;
        private System.Windows.Forms.Label green;
        private System.Windows.Forms.Label greenValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label blueValue;
        private System.Windows.Forms.Label valueValue;
        private System.Windows.Forms.Label value;
        private System.Windows.Forms.Label saturationValue;
        private System.Windows.Forms.Label saturation;
        private System.Windows.Forms.Label hueValue;
        private System.Windows.Forms.Label hue;
        private System.Windows.Forms.Label yellowValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label magentaValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label cyanValue;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label keyValue;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label vValue;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label uValue;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label yValue;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label xPosition;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label yPosition;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.RadioButton colorChannel4;
        private System.Windows.Forms.RadioButton colorChannel3;
        private System.Windows.Forms.RadioButton colorChannel2;
        private System.Windows.Forms.RadioButton colorChannel1;
        private System.Windows.Forms.RadioButton compositeColors;
        private System.Windows.Forms.DataVisualization.Charting.Chart histogramOriginal;
        private System.Windows.Forms.DataVisualization.Charting.Chart histogramNew;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drawingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectColorToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton normalize;
        private System.Windows.Forms.RadioButton stretch;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label normalizeValueLabel;
        private System.Windows.Forms.Label normalizeLabel;
        private System.Windows.Forms.TrackBar normalizeValue;
        private System.Windows.Forms.ToolStripMenuItem filtersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blurlinearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blur19ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blur110ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blur116ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mediannonlinearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem median3x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem median5x5ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem median7x7ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharpenlinearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharpen1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharpen2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharpen3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem segmentationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edgeDetectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem soboelOperator3x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rGBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intensityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prewittOperator3x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rGBToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem redToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem greenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem blueToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem intensityToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem robertsOperator2x2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rGBToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem redToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem greenToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem blueToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem intensityToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem verticalAdaptiveThresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redVerticalAdaptiveThreshold;
        private System.Windows.Forms.ToolStripMenuItem greenVerticalAdaptiveThreshold;
        private System.Windows.Forms.ToolStripMenuItem blueVerticalAdaptiveThreshold;
        private System.Windows.Forms.ToolStripMenuItem intensityVerticalAdaptiveThreshold;
        private System.Windows.Forms.ToolStripMenuItem verticalManualThresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horizontalManualThresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem twoVerticalManualThresholdsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redVerticalManualThreshold;
        private System.Windows.Forms.ToolStripMenuItem greenVerticalManualThreshold;
        private System.Windows.Forms.ToolStripMenuItem blueVerticalManualThreshold;
        private System.Windows.Forms.ToolStripMenuItem intensityVerticalManualThreshold;
        private System.Windows.Forms.ToolStripMenuItem redHorizontalManualThreshold;
        private System.Windows.Forms.ToolStripMenuItem greenHorizontalManualThreshold;
        private System.Windows.Forms.ToolStripMenuItem blueHorizontalManualThreshold;
        private System.Windows.Forms.ToolStripMenuItem intensityHorizontalManualThreshold;
        private System.Windows.Forms.ToolStripMenuItem redTwoVerticalManualThresholds;
        private System.Windows.Forms.ToolStripMenuItem greenTwoVerticalManualThresholds;
        private System.Windows.Forms.ToolStripMenuItem blueTwoVerticalManualThresholds;
        private System.Windows.Forms.ToolStripMenuItem intensityTwoVerticalManualThresholds;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label threshold1Value;
        private System.Windows.Forms.Label threshold1Label;
        private System.Windows.Forms.TrackBar thresholdTrackbar1;
        private System.Windows.Forms.TrackBar thresholdTrackbar2;
        private System.Windows.Forms.Label threshold2Value;
        private System.Windows.Forms.Label threshold2Label;
        private System.Windows.Forms.ToolStripMenuItem transitionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transformationsToolStripMenuItem;
    }
}

