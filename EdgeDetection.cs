﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ikaa_8_ls_171rdb133
{
    public class EdgeDetection
    {
        public int[,] gX;
        public int[,] gY;
        public int size;

        public EdgeDetection(string type)
        {
            if (type == "sobel")
            {
                size = 3;
                gX = new int[,] { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
                gY = new int[,] { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };
            }
            if (type == "prewitt")
            {
                size = 3;
                gX = new int[,] { { -1, 0, 1 }, { -1, 0, 1 }, { -1, 0, 1 } };
                gY = new int[,] { { 1, 1, 1 }, { 0, 0, 0 }, { -1, -1, -1 } };
            }
            if (type == "roberts")
            {
                size = 2;
                gX = new int[,] { { -1, 0 }, { 0, 1 } };
                gY = new int[,] { { 1, 0 }, { 0, -1 } };
            }
        }
    }
}
