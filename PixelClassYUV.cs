﻿namespace ikaa_8_ls_171rdb133
{
    public class PixelClassYUV
    {
        public double Y;

        public double U;

        public double V;

        public PixelClassYUV()
        {
            Y = 0;
            U = 0;
            V = 0;
        }

        public PixelClassYUV(byte R, byte G, byte B)
        {
            Y = 0.299 * R + 0.587 * G + 0.114 * B;
            U = -0.14713 * R - 0.28886 * G + 0.436 * B + 128;
            V = 0.615 * R - 0.51499 * G - 0.10001 * B + 128;
        }
    }
}
