﻿using System;
using System.Windows.Forms;

namespace ikaa_8_ls_171rdb133
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LS_171rdb133());
        }
    }
}
