﻿using System;

namespace ikaa_8_ls_171rdb133
{
    public class PixelClassCMYK
    {
        public double C; // Cyan

        public double M; // Magenta

        public double Y; // Yellow

        public double K; // Black

        public PixelClassCMYK()
        {
            C = 0;
            M = 0;
            Y = 0;
            K = 0;
        }

        public PixelClassCMYK(byte R, byte G, byte B)
        {
            double rPrim = (double)R / 255;
            double gPrim = (double)G / 255;
            double bPrim = (double)B / 255;

            K = 1 - Math.Max(Math.Max(rPrim, gPrim), bPrim);

            C = (1 - rPrim - K) / (1 - K);
            M = (1 - gPrim - K) / (1 - K);
            Y = (1 - bPrim - K) / (1 - K);
        }
    }
}
