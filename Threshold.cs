﻿using ikaa_8_ls_171rdb133;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ikaa_8_ls_171rdb133
{
    public class Threshold
    {
        private ImgData imgData;
        private string mode;
        public double[] histogram;

        public Threshold(ImgData imgData, string mode)
        {
            this.imgData = imgData;
            this.mode = mode;
            histogram = GetHistogram();
        }

        public int Tlow()
        {
            int rangeStart = imgData.CalculateRangeStart(histogram);
            int rangeEnd = imgData.CalculateRangeEnd(histogram);
            int t = (rangeEnd - rangeStart) / 2;
            int previousT = 0;
            while (t != previousT)
            {
                previousT = t;
                int m1 = 0, m2 = 0, p1 = 0, p2 = 0;

                for (int i = rangeStart; i < t; i++)
                {
                    p1 += (int)histogram[i];
                    m1 += (int)(i * histogram[i]);
                }
                if (p1 == 0)
                {
                    p1 = 1;
                }

                m1 /= p1;
                for (int i = t; i <= rangeEnd; i++)
                {
                    p2 += (int)histogram[i];
                    m2 += (int)(i * histogram[i]);
                }
                if (p2 == 0)
                {
                    p2 = 1;
                }
                m2 /= p2;
                t = (m1 + m2) / 2;
            }
            return t;
        }

        private double[] GetHistogram()
        {
            if (this.mode == "RED")
            {
                return imgData.histoRed;
            }
            if (this.mode == "GREEN")
            {
                return imgData.histoGreen;
            }
            if (this.mode == "BLUE")
            {
                return imgData.histoBlue;
            }
            return imgData.histoIntensity;
        }
    }
}
